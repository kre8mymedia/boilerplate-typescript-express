import supertest from 'supertest';
import app from '../../src/index';

describe('Project routes...', () => {
  let token: string;
  let projectId: string;

  const request = supertest(app);

  jest.setTimeout(2000);

  beforeAll((done) => {
    request
      .post('/api/v1/login')
      .send({
        email: 'tester@gmail.com',
        password: 'test1234',
      })
      .end(function (err, res) {
        token = res.body.token; // Or something
        done();
      });
  });

  test('receive 200 from GET /api/v1/projects/all', function (done) {
    request
      .get('/api/v1/events')
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + token)
      .expect(200, done);
  });

  test('receive 201 from POST /api/v1/projects', function (done) {
    const createData = {
      name: 'Construction Project',
      color: '#000000',
    };
    request
      .post('/api/v1/projects')
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + token)
      .send(createData)
      .end(function (err, res) {
        if (err) {
          console.log(err);
        } else {
          expect(res.statusCode).toBe(201);
          projectId = res.body.project._id;
          //   console.log('eventId: ', eventId);
          done();
        }
      });
  });

  test('receive (single resource) 200 from GET /api/v1/projects', function (done) {
    request
      .get(`/api/v1/projects`)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + token)
      .expect(200, done);
  });

  // Broken
  test('receive 200 from PUT /api/v1/projects/{id}', function (done) {
    const updateData = {
      name: 'Work Project',
      color: '#8D8D8D',
    };
    request
      .put(`/api/v1/projects/${projectId}`)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + token)
      .send(updateData)
      .end(function (err, res) {
        if (err) {
          console.log(err);
        } else {
          expect(res.statusCode).toBe(200);
          expect(res.body.project).toHaveProperty('name', updateData.name);
          done();
        }
      });
  });

  // Broken
  test(`receive 200 from DELETE /api/v1/projects/{id}`, function (done) {
    request
      .delete(`/api/v1/projects/${projectId}`)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + token)
      .expect(200, done);
  });
});
