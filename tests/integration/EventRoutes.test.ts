import supertest from 'supertest';
import app from '../../src/index';

describe('Event routes...', () => {
  let token: string;
  let eventId: string;
  const request = supertest(app);

  jest.setTimeout(10000);

  beforeAll((done) => {
    request
      .post('/api/v1/login')
      .send({
        email: 'tester@gmail.com',
        password: 'test1234',
      })
      .end(function (err, res) {
        token = res.body.token; // Or something
        done();
      });
  });

  test('receive 200 from GET /api/v1/events', function (done) {
    request
      .get('/api/v1/events')
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + token)
      .expect(200, done);
  });

  test('receive 201 from POST /api/v1/events', function (done) {
    const createData = {
      title: 'Ski vacation',
      start: 1652716800000,
      end: 1652916800000,
      bgColor: '#8C8C8C',
      location: '8302 S Brighton Loop Rd, Brighton, UT 84121',
      description:
        'Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    };
    request
      .post('/api/v1/events')
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + token)
      .send(createData)
      .end(function (err, res) {
        if (err) {
          console.log(err);
        } else {
          expect(res.statusCode).toBe(201);
          eventId = res.body.event._id;
          //   console.log('eventId: ', eventId);
          done();
        }
      });
  });

  test('receive 200 from GET /api/v1/events/{id}', function (done) {
    request
      .get(`/api/v1/events/${eventId}`)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + token)
      .expect(200, done);
  });

  test('receive 200 from PUT /api/v1/events/{id}', function (done) {
    const updateData = {
      title: 'Work trip',
      start: 1652416800000,
      end: 1652816800000,
      bgColor: '#8C8C8C',
      location: '8302 S Weston Rd, Amarillo, TX 85487',
      description:
        'Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    };
    request
      .put(`/api/v1/events/${eventId}`)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + token)
      .send(updateData)
      .end(function (err, res) {
        if (err) {
          console.log(err);
        } else {
          expect(res.statusCode).toBe(200);
          expect(res.body.event).toHaveProperty('title', updateData.title);
          //   console.log('eventId: ', eventId);
          done();
        }
      });
  });

  test(`receive 200 from DELETE /api/v1/events/{id}`, function (done) {
    request
      .delete(`/api/v1/events/${eventId}`)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + token)
      .expect(200, done);
  });
});
