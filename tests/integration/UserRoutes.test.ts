import axios from 'axios';
import { HOST } from '../../src/config/config';

test.skip('Can list users...', async () => {
  const res = await axios.get(`${HOST}/api/v1/users`);
  expect(res.data.users.length).toBeGreaterThan(0);
});
