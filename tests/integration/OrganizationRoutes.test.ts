import supertest from 'supertest';
import app from '../../src/index';

describe('Organization routes...', () => {
  let token: string;
  let orgId: string;

  const request = supertest(app);

  jest.setTimeout(2000);

  beforeAll((done) => {
    request
      .post('/api/v1/login')
      .send({
        email: 'tester@gmail.com',
        password: 'test1234',
      })
      .end(function (err, res) {
        token = res.body.token; // Or something
        done();
      });
  });

  test('receive 200 from GET /api/v1/organizations/all', function (done) {
    request
      .get('/api/v1/events')
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + token)
      .expect(200, done);
  });

  test('receive 201 from POST /api/v1/organizations', function (done) {
    const createData = {
      name: 'Capsule Corp',
      slug: 'capsule-corp',
    };
    request
      .post('/api/v1/organizations')
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + token)
      .send(createData)
      .end(function (err, res) {
        if (err) {
          console.log(err);
        } else {
          expect(res.statusCode).toBe(201);
          orgId = res.body.organization._id;
          //   console.log('eventId: ', eventId);
          done();
        }
      });
  });

  test('receive (single resource) 200 from GET /api/v1/organizations', function (done) {
    request
      .get(`/api/v1/organizations`)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + token)
      .expect(200, done);
  });

  test('receive 200 from PUT /api/v1/organizations/{id}', function (done) {
    const updateData = { name: 'UpdatedTestOrg' };
    request
      .put(`/api/v1/organizations/${orgId}`)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + token)
      .send(updateData)
      .end(function (err, res) {
        if (err) {
          console.log(err);
        } else {
          expect(res.statusCode).toBe(200);
          expect(res.body.organization).toHaveProperty('name', updateData.name);
          done();
        }
      });
  });

  test(`receive 200 from DELETE /api/v1/organizations/{id}`, function (done) {
    request
      .delete(`/api/v1/organizations/${orgId}`)
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + token)
      .expect(200, done);
  });
});
