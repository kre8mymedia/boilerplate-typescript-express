import PriceService, {
  CreatePriceDto,
} from '../../../../src/services/StripeService/Price';
import ProductService, {
  ProductDto,
} from '../../../../src/services/StripeService/Product';
import Stripe from 'stripe';

import stripeConfig from '../../../../src/config/stripe';

const TEST_SECRET: string = stripeConfig.env.secretKey || '';

describe('Stripe PriceService can...', () => {
  const priceService = new PriceService(TEST_SECRET);
  const productService = new ProductService(TEST_SECRET);
  const priceDto: CreatePriceDto = {
    unit_amount: 2000,
    currency: 'usd',
    recurring: { interval: 'month' },
    product: '',
  };
  const mockItem: ProductDto = {
    name: 'TESTPRODUCT: Gold Special 134',
    images: [
      'https://siasky.net/fAMBPSTxDeD53Xf6mGoStmQs3_EoJM6YaI-eR9sCiVAhbw',
      'https://siasky.net/PAAuG0_LhXRcCxchQhctq705zqxwId531mzj85UwQjMrRQ',
      'https://siasky.net/PAAf58tfBooSprLaOYEhtNWNbSxJ4H6_llVrkF9hKwiCyA',
    ],
    url: 'https://dev-calendar.glootie.ml',
  };
  // let itemId: string;
  let mockProduct: Stripe.Product;
  let testPrice: Stripe.Price;

  beforeAll(async () => {
    mockProduct = await productService.create(mockItem);
    priceDto.product = mockProduct.id;
  });

  afterAll(async () => {
    await priceService.update(testPrice.id, { active: false });
    await productService.delete(mockProduct.id);
  });

  test('create price resource', async () => {
    const item = await priceService.create(priceDto);
    testPrice = item;
    // console.log('PriceService.test.create: ', testPrice);
    expect(item.active).toBe(true);
  });

  test('list price resources', async () => {
    const limit = 3;
    const items = await priceService.list({ limit });
    // console.log('PriceService.test.list: ', items);
    expect(items.data.length).toBeGreaterThanOrEqual(1);
  });

  test('retrieve price resource', async () => {
    const item = await priceService.retrieve(testPrice.id);
    // console.log('PriceService.test.retrieve: ', item);
    expect(item.active).toBe(true);
  });

  test('update price resource', async () => {
    const item = await priceService.update(testPrice.id, {
      active: true,
      nickname: 'update resource',
    });
    expect(item.active).toBe(true);
    expect(item.nickname).toBe('update resource');
  });
});
