import ProductService, {
  ProductDto,
} from '../../../../src/services/StripeService/Product';

import stripeConfig from '../../../../src/config/stripe';

const TEST_SECRET: string = stripeConfig.env.secretKey || '';

describe('StripeService can...', () => {
  const productService = new ProductService(TEST_SECRET);
  let itemId: string;
  const mockItem: ProductDto = {
    name: 'TESTPRODUCT: Gold Special 134',
    images: [
      'https://siasky.net/fAMBPSTxDeD53Xf6mGoStmQs3_EoJM6YaI-eR9sCiVAhbw',
      'https://siasky.net/PAAuG0_LhXRcCxchQhctq705zqxwId531mzj85UwQjMrRQ',
      'https://siasky.net/PAAf58tfBooSprLaOYEhtNWNbSxJ4H6_llVrkF9hKwiCyA',
    ],
    url: 'https://dev-calendar.glootie.ml',
  };

  test('create product resource', async () => {
    const item = await productService.create(mockItem);
    itemId = item.id;
    expect(item).toHaveProperty('name', mockItem.name);
    expect(item.images.length).toBeGreaterThanOrEqual(1);
    expect(item).toHaveProperty('url', mockItem.url);
  });

  test('list product resources', async () => {
    const limit = 3;
    const items = await productService.list({ limit });
    // console.log('Product.test.list: ', items);
    expect(items.data.length).toBeGreaterThanOrEqual(1);
  });

  test('retrieve product resource', async () => {
    const item = await productService.retrieve(itemId);
    expect(item).toHaveProperty('name', mockItem.name);
    expect(item.images.length).toBeGreaterThanOrEqual(1);
    expect(item).toHaveProperty('url', mockItem.url);
  });

  test('delete product resource', async () => {
    const item = await productService.delete(itemId);
    expect(item).toBe(true);
  });
});
