import Broker from '../../../src/services/broker';

describe('Broker service can...', () => {
  const broker = new Broker();

  test('publish data json to topic', () => {
    const data = { title: 'Hello from MongoDB', author: 'Ryan Eggleston' };
    try {
      const pub = broker.publish('books', JSON.stringify(data));
      expect(pub).toBe(true);
    } catch (e) {
      console.log(e);
    }
  });

  // test('publish data json to topic', () => {
  //   const val = true;
  //   expect(val).toBe(true);
  // });
});
