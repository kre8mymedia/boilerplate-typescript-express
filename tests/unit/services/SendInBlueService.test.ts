import { SEND_IN_BLUE_API_KEY } from '../../../src/config/config';
import SendInBlueService, {
  REGISTERED_USER_LIST_ID,
} from '../../../src/services/SendInBlueService';

const sendInBlueService = new SendInBlueService(SEND_IN_BLUE_API_KEY);

type TestEmailResponse = {
  id: number;
};

describe('SendInBlueService can...', () => {
  const mockEmail = {
    email: 'kre8mymedia@gmail.com',
    listIds: [REGISTERED_USER_LIST_ID],
    updateEnabled: true,
  };
  let testEmail: TestEmailResponse;

  test('create contact resource', async () => {
    const item = await sendInBlueService.createContact(mockEmail);
    testEmail = item?.data;
    // console.log('SendInBlueService.test.createContact: ', item?.data);
    expect(item?.data).toHaveProperty('id', testEmail.id);
  });

  test('delete contact resource', async () => {
    const item = await sendInBlueService.deleteContact(testEmail.id);
    // console.log('SendInBlueService.test.deleteContact: ', item);
    expect(item?.data).toBe('');
  });
});
