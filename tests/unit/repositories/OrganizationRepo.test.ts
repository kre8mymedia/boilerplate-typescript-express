import mongoose from 'mongoose';
// DB Config
import dbConfig from '.../../../src/config/config';
// Repositories
import OrganizationRepo from '../../../src/organizations/OrganizationRepo';
import UserRepo from '../../../src/users/UserRepo';
// Interfaces
import IUser from '../../../src/users/UserInterface';
import { IOrganization } from '../../../src/organizations/OrganizationInterface';

const orgRepo: OrganizationRepo = new OrganizationRepo();
const userRepo: UserRepo = new UserRepo();

const TEST_USER_ID = '6301b636b510b606f94ff6e0';

const fetchAuthUser = async () => {
  const authUser = await userRepo.findById(TEST_USER_ID);
  return authUser;
};

jest.setTimeout(2000);

describe('OrganizationRepo can...', () => {
  let authUser: IUser;
  let testItem: IOrganization;

  beforeAll(async () => {
    await mongoose.connect(dbConfig.mongo.url);
    authUser = await fetchAuthUser();
  });

  afterAll(async () => {
    await mongoose.connection.close();
  });

  test('create item', async () => {
    const itemData = {
      primaryUser: authUser._id,
      name: 'Example Organization',
      slug: 'example-organization',
    };

    try {
      const item = await orgRepo.create(itemData);
      // console.log(event);
      testItem = item;
      expect(item).toHaveProperty('name', 'Example Organization');
    } catch (e) {
      throw new Error(e);
    }
  });

  test('list items', async () => {
    try {
      const items = await orgRepo.list();
      // console.log(events);
      expect(items.length).toBeGreaterThanOrEqual(1);
    } catch (e) {
      throw new Error(e);
    }
  });

  test('read item', async () => {
    try {
      const item = await orgRepo.findById(testItem._id);
      // console.log(item);
      expect(item).toHaveProperty('name', 'Example Organization');
    } catch (e) {
      throw new Error(e);
    }
  });

  test('update item', async () => {
    const updatedData = {
      name: 'Test Organization',
      tokenData: { _id: authUser._id },
    };

    try {
      const item = await orgRepo.update(testItem._id, updatedData);
      // console.log(item);
      expect(item).toHaveProperty('name', 'Test Organization');
    } catch (e) {
      throw new Error(e);
    }
  });

  test('delete item', async () => {
    try {
      const item = await orgRepo.delete(testItem._id, { _id: authUser._id });
      // console.log(item);
      expect(item).toBe(true);
    } catch (e) {
      throw new Error(e);
    }
  });
});
