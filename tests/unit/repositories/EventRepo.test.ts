import mongoose from 'mongoose';
// DB Config
import dbConfig from '.../../../src/config/config';
// Repositories
import EventRepo from '../../../src/events/EventRepo';
import UserRepo from '../../../src/users/UserRepo';
import NotificationRepo from '../../../src/notifications/NotificationRepo';
import ProjectRepo from '../../../src/projects/ProjectRepo';
// Interfaces
import IUser from '../../../src/users/UserInterface';
import { IEvent } from '../../../src/events/EventInterface';
import { INotification } from '../../../src/notifications/NotificationInterface';
import { IProject } from '../../../src/projects/ProjectInterface';
// Models
// import Event from '../../../src/events/Event';
// import Project from '../../../src/projects/Project';
// import Notification from '../../../src/notifications/Notification';

const eventRepo: EventRepo = new EventRepo();
const userRepo: UserRepo = new UserRepo();
const notificationRepo: NotificationRepo = new NotificationRepo();
const projectRepo: ProjectRepo = new ProjectRepo();

const TEST_USER_ID = '6301b636b510b606f94ff6e0';

const fetchAuthUser = async () => {
  const authUser = await userRepo.findById(TEST_USER_ID);
  return authUser;
};

jest.setTimeout(5000);

describe('EventRepo can...', () => {
  let authUser: IUser;
  let testEvent: IEvent;
  let initialNotification: INotification;
  let updateNotification: INotification;
  let testProject: IProject;

  beforeAll(async () => {
    await mongoose.connect(dbConfig.mongo.url);

    authUser = await fetchAuthUser();
    initialNotification = await notificationRepo.create({
      name: 'TEST Slack Notification',
      hook: 'https://hooks.slack.com/services/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',
      type: 'slack',
      tokenData: { _id: authUser._id },
    });

    updateNotification = await notificationRepo.create({
      name: 'TEST Email Notification',
      hook: 'kre8mymedia@gmail.com',
      type: 'email',
      tokenData: { _id: authUser._id },
    });

    testProject = await projectRepo.create({
      name: 'Test Case Event Project',
      color: '#000000',
      tokenData: { _id: authUser._id },
    });
  });

  afterAll(async () => {
    // console.log('testEvent: ', testEvent._id.toString());
    await notificationRepo.delete(initialNotification._id, {
      _id: authUser._id,
    });
    await notificationRepo.delete(updateNotification._id, {
      _id: authUser._id,
    });
    await projectRepo.delete(testProject._id, { _id: authUser._id });
    await mongoose.connection.close();
  });

  test('create item', async () => {
    const eventData = {
      user: authUser._id,
      title: 'Test event',
      start: 1652716800000,
      end: 1652719900000,
      description: 'hello from descpription',
      location: '4888 Highland Cir. Holladay, UT 23458',
      notifications: [initialNotification._id],
      project: testProject._id,
    };
    const item = await eventRepo.create(eventData);
    testEvent = item;
    // console.log('EventRepo.createItem: ', eventData);
    expect(item).toHaveProperty('title', eventData.title);
    expect(item.project).toBe(testProject._id);
    expect(item.notifications[0]).toHaveProperty(
      '_id',
      initialNotification._id
    );
  });

  test('read item', async () => {
    try {
      const item = await eventRepo.findById(testEvent._id);
      // console.log('EventRepo.readItem: ', item);
      expect(item).toHaveProperty('title', 'Test event');
    } catch (e) {
      throw new Error(e);
    }
  });

  test('update item', async () => {
    const updatedData = {
      title: 'Test Test',
      description: 'hello from updated description',
      location: '6901 Sumner St. Dallas, TX 75024',
      tokenData: { _id: authUser._id },
      notifications: [updateNotification._id],
    };

    const item = await eventRepo.update(testEvent._id, updatedData);
    // console.log(item);
    expect(item).toHaveProperty('title', updatedData.title);
    expect(item).toHaveProperty(
      'description',
      'hello from updated description'
    );
    // console.log('EventRepo.updateItem: ', item);
    expect(item).toHaveProperty('location', '6901 Sumner St. Dallas, TX 75024');
    expect(item?.notifications[0]).toHaveProperty(
      '_id',
      updateNotification._id
    );
  });

  test('list items', async () => {
    const items = await eventRepo.list();
    // console.log('EventRepo.listItems: ', items);
    expect(items.length).toBeGreaterThanOrEqual(1);
  });

  test('list items by user', async () => {
    try {
      const items = await eventRepo.listByUser({ _id: authUser._id });
      // console.log(items);
      expect(items.length).toBeGreaterThanOrEqual(1);
    } catch (e) {
      throw new Error(e);
    }
  });

  test('delete item', async () => {
    try {
      const item = await eventRepo.delete(testEvent._id, { _id: authUser._id });
      // console.log('EventRepo.deleteItem: ', testEvent);
      expect(item).toBe(true);
    } catch (e) {
      throw new Error(e);
    }
  });
});
