import mongoose from 'mongoose';
// DB Config
import dbConfig from '.../../../src/config/config';
// Repositories
import ProjectRepo from '../../../src/projects/ProjectRepo';
import UserRepo from '../../../src/users/UserRepo';
// Interfaces
import IUser from '../../../src/users/UserInterface';
import { IProject } from '../../../src/projects/ProjectInterface';
// Models
import Project from '../../../src/projects/Project';

const projectRepo: ProjectRepo = new ProjectRepo();
const userRepo: UserRepo = new UserRepo();

const TEST_USER_ID = '6301b636b510b606f94ff6e0';

const fetchAuthUser = async () => {
  const authUser = await userRepo.findById(TEST_USER_ID);
  return authUser;
};

jest.setTimeout(2000);

describe('ProjectRepo can...', () => {
  let authUser: IUser;
  let testItem: IProject;

  beforeAll(async () => {
    await mongoose.connect(dbConfig.mongo.url);
    authUser = await fetchAuthUser();
  });

  afterAll(async () => {
    await Project.collection.drop();
    await mongoose.connection.close();
  });

  test('create item', async () => {
    const itemData = {
      tokenData: { _id: authUser._id },
      name: 'Example Project',
      color: '#000000',
    };

    try {
      const item = await projectRepo.create(itemData);
      // console.log(event);
      testItem = item;
      expect(item).toHaveProperty('name', 'Example Project');
    } catch (e) {
      throw new Error(e);
    }
  });

  test('list items', async () => {
    try {
      const items = await projectRepo.list();
      // console.log(events);
      expect(items.length).toBeGreaterThanOrEqual(1);
    } catch (e) {
      throw new Error(e);
    }
  });

  test('read item', async () => {
    try {
      const item = await projectRepo.findById(testItem._id);
      // console.log(item);
      expect(item).toHaveProperty('name', 'Example Project');
    } catch (e) {
      throw new Error(e);
    }
  });

  test('update item', async () => {
    const updatedData = {
      name: 'Test Project',
      color: '#FF0000',
      tokenData: { _id: authUser._id },
    };

    const item = await projectRepo.update(testItem._id, updatedData);
    // console.log(item);
    expect(item).toHaveProperty('name', updatedData.name);
    expect(item).toHaveProperty('color', updatedData.color);
  });

  test('delete item', async () => {
    try {
      const item = await projectRepo.delete(testItem._id, {
        _id: authUser._id,
      });
      // console.log(item);
      expect(item).toBe(true);
    } catch (e) {
      throw new Error(e);
    }
  });
});
