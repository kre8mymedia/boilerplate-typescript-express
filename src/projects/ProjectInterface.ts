import { Document, Types } from 'mongoose';
import { IEvent } from '../events/EventInterface';
// Types
import { TokenData } from '../utils/types';

export interface IProject extends Document {
  name: string;
  color: string;
  organization: Types.ObjectId;
  user: Types.ObjectId;
  events: IEvent[];
}

export type ProjectInput = {
  name: string;
  color: string;
  tokenData?: TokenData;
};
