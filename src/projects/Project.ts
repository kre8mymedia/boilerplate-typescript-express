import { IProject } from './ProjectInterface';
import mongoose, { Schema } from 'mongoose';
import { IEvent } from '../events/EventInterface';
import Event from '../events/Event';

const ProjectSchema: Schema = new Schema(
  {
    name: { type: String, required: true },
    color: { type: String },
    organization: { type: Schema.Types.ObjectId, ref: 'Organization' },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    events: [{ type: Schema.Types.ObjectId, ref: Event }],
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: false },
    id: false,
  }
);

ProjectSchema.set('versionKey', false);
ProjectSchema.index({ name: 1, user: 1 }, { unique: true });
ProjectSchema.virtual('totalHours').get(function () {
  const sum: number = this.events
    .map((event: IEvent) => event.hours)
    .reduce((prev: number, curr: number) => prev + curr, 0);
  return this.events.length > 0 ? sum : 0;
});
ProjectSchema.pre('find', function (next) {
  this.populate('events');
  next();
});

export default mongoose.model<IProject>('Project', ProjectSchema);
