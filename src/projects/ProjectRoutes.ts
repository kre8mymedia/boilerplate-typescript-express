import express from 'express';
import { authMiddleware } from '../middleware/auth';
import controller from '../projects/ProjectController';

const router = express.Router();

router.get('/projects', authMiddleware, controller.list);
router.post('/projects', authMiddleware, controller.create);
router.get('/projects/:id', authMiddleware, controller.find);
router.put('/projects/:id', authMiddleware, controller.update);
router.patch('/projects/:id', authMiddleware, controller.update);
router.delete('/projects/:id', authMiddleware, controller.remove);

export default router;
