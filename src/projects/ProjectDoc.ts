/**
 * --------------------------------------------------------------------------------------
 * Project Schema
 * --------------------------------------------------------------------------------------
 * @openapi
 * components:
 *   schemas:
 *     Project:
 *       type: object
 *       properties:
 *         _id:
 *           type: uid
 *           description: The project ID.
 *           example: 62f94d2d0890a74abca873ca
 *         name:
 *           type: string
 *           description: Name of the project
 *           example: Construction Project
 *         color:
 *           type: string
 *           description: Hex color for event color labels
 *           example: "#000000"
 *         hours:
 *           type: integer
 *           description: Total hours of events allocated
 *           example: 120
 */

/**
 * --------------------------------------------------------------------------------------
 * Project with Events Schema
 * --------------------------------------------------------------------------------------
 * @openapi
 * components:
 *   schemas:
 *     ProjectWithEvents:
 *       type: object
 *       properties:
 *         _id:
 *           type: uid
 *           description: The project ID.
 *           example: 62f94d2d0890a74abca873ca
 *         name:
 *           type: string
 *           description: Name of the project
 *           example: Construction Project
 *         color:
 *           type: string
 *           description: Hex color for event color labels
 *           example: "#000000"
 *         hours:
 *           type: integer
 *           description: Total hours of events allocated
 *           example: 120
 *         events:
 *           items:
 *             $ref: '#/components/schemas/Event'
 */

/**
 * --------------------------------------------------------------------------------------
 * List projects
 * --------------------------------------------------------------------------------------
 * @openapi
 * /projects:
 *   get:
 *     tags:
 *       - Projects
 *     summary: List Projects
 *     description: List all projects that exist
 *     responses:
 *       200:
 *         description: A list of projects.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  success:
 *                      type: boolean
 *                      example: true
 *                  projects:
 *                      items:
 *                        $ref: '#/components/schemas/Project'
 */

/**
 * --------------------------------------------------------------------------------------
 * Create Project
 * --------------------------------------------------------------------------------------
 *
 * @openapi
 * /projects:
 *   post:
 *     security:
 *      - bearerAuth: []
 *     tags:
 *       - Projects
 *     summary: Create Project
 *     description: creates a new project
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: Name of the project
 *                 example: Construction Project
 *               color:
 *                 type: string
 *                 description: Hex color for event color labels
 *                 example: "#000000"
 *     responses:
 *       201:
 *         description: Creates project
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  success:
 *                      type: boolean
 *                      example: true
 *                  project:
 *                      type: object
 *                      $ref: '#/components/schemas/Project'
 *       422:
 *         description: Validation error
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/ValidationError'
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/NotFoundResponse'
 */

/**
 * --------------------------------------------------------------------------------------
 * Fetch Project by ID
 * --------------------------------------------------------------------------------------
 * @openapi
 * /projects/{id}:
 *   get:
 *     tags:
 *       - Projects
 *     summary: Retreive Project
 *     description: retrieve project
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: ID of the project to fetch resource
 *     responses:
 *       200:
 *         description: A single project.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  success:
 *                      type: boolean
 *                      example: true
 *                  project:
 *                      type: object
 *                      $ref: '#/components/schemas/ProjectWithEvents'
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/NotFoundResponse'
 */

/**
 * --------------------------------------------------------------------------------------
 * Update Project by ID
 * --------------------------------------------------------------------------------------
 * @openapi
 * /projects/{id}:
 *   put:
 *     tags:
 *       - Projects
 *     summary: Update Project by ID
 *     description: retrieve project by id
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: ID of the project to fetch resource
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: Name of the project
 *                 example: Construction Project 2.0
 *               color:
 *                 type: string
 *                 description: Hex color for event color labels
 *                 example: "#212424"
 *     responses:
 *       200:
 *         description: Update single project.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  success:
 *                      type: boolean
 *                      example: true
 *                  project:
 *                      type: object
 *                      $ref: '#/components/schemas/Project'
 *       422:
 *         description: Validation error
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/ValidationError'
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/NotFoundResponse'
 */

/**
 * --------------------------------------------------------------------------------------
 * Delete Project by ID
 * --------------------------------------------------------------------------------------
 * @openapi
 * /projects/{id}:
 *   delete:
 *     tags:
 *       - Projects
 *     summary: Delete Project by ID
 *     description: Delete project by id
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: ID of the project to fetch resource
 *     responses:
 *       200:
 *         description: Delete single project.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  success:
 *                      type: boolean
 *                      example: true
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/NotFoundResponse'
 */
