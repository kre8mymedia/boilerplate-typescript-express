import { LeanDocument, Types } from 'mongoose';

// Models
import Project from './Project';
// Types
import { TokenData } from '../utils/types';
// Interfaces
import { IProject, ProjectInput } from '../projects/ProjectInterface';
// Repositories
import Repository from '../interfaces/RepositoryInterface';
import UserRepo from '../users/UserRepo';

const userRepo: UserRepo = new UserRepo();

export default class ProjectRepo implements Repository<IProject> {
  public async list(): Promise<IProject[]> {
    try {
      const list = await Project.find()
        .populate('events', ['-project', '-user', '-createdAt', '-updatedAt'])
        .select(['name', 'color', '_id', 'totalHours'])
        .exec();
      return list;
    } catch (e) {
      console.log(e);
    }
  }

  public async listByUser(data: TokenData): Promise<
    Omit<
      LeanDocument<
        IProject & {
          _id: Types.ObjectId;
        }
      >,
      never
    >[]
  > {
    try {
      const list = await Project.find({ user: data._id })
        .populate('events', ['-project', '-user', '-createdAt', '-updatedAt'])
        .select(['name', 'color', '_id', 'totalHours'])
        .exec();
      return list;
    } catch (e) {
      console.log(e);
    }
  }

  // public async listByOrg(data: TokenData): Promise<IProject[]> {
  //   try {
  //     const list = await Project.find({ organization: data._id });
  //     return list;
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }

  public async findById(id: string): Promise<IProject> {
    try {
      const model = await Project.findOne({ _id: id }).populate('events', [
        '-project',
        '-user',
        '-createdAt',
        '-updatedAt',
      ]);
      return model;
    } catch (e) {
      console.log(e);
    }
  }

  public async create({ ...input }: ProjectInput): Promise<IProject> {
    const authUser = await userRepo.findById(input.tokenData._id);

    const model = new Project({
      _id: new Types.ObjectId(),
      ...input,
      organization: authUser.organization,
      user: authUser._id,
    });
    const result = await model.save();
    return result;
  }

  public async update(
    id: string,
    data: ProjectInput
  ): Promise<IProject | null> {
    const project = await Project.findById(id);
    if (!project) return null;
    const authUser = await userRepo.findById(data.tokenData._id);
    // Check if org exists
    if (project.organization === null) {
      if (project.user._id.toString() !== authUser._id.toString()) {
        return null;
      }
    } else {
      if (
        project.organization._id.toString() !== authUser.organization.toString()
      ) {
        return null;
      }
    }

    await Project.findByIdAndUpdate(id, data);
    const result = await Project.findById(id);
    if (!result) return null;
    return result;
  }

  public async delete(
    id: string,
    data: { _id: string; email?: string }
  ): Promise<boolean> {
    const item = await Project.findById(id);
    if (!item) return null;
    const authUser = await userRepo.findById(data._id);

    if (item.user._id.toString() !== authUser._id.toString()) {
      return null;
    }

    try {
      await item.delete();
      return true;
    } catch (err) {
      console.log(err);
      return false;
    }
  }
}
