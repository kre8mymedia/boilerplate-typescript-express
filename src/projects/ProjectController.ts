import { Request, Response } from 'express';

import ProjectRepo from './ProjectRepo';

const projectRepo: ProjectRepo = new ProjectRepo();

class ProjectController {
  public static list = async (req: Request, res: Response) => {
    try {
      const projects = await projectRepo.listByUser(req.body.tokenData);
      return res.status(200).json({
        success: true,
        projects,
      });
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static create = async (req: Request, res: Response) => {
    const { name, color, tokenData } = req.body;

    const project = await projectRepo.create({
      name,
      color,
      tokenData: tokenData,
    });

    try {
      const result = await project.save();
      return res.status(201).json({
        success: true,
        project: result,
      });
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static find = async (req: Request, res: Response) => {
    try {
      const project = await projectRepo.findById(req.params.id);
      if (!project) {
        return res.status(404).json({
          success: false,
        });
      } else {
        return res.status(200).json({
          success: true,
          project: project,
        });
      }
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static update = async (req: Request, res: Response) => {
    try {
      const project = await projectRepo.update(req.params.id, req.body);

      if (!project) {
        return res.status(404).json({
          success: false,
        });
      } else {
        return res.status(200).json({
          success: true,
          project: project,
        });
      }
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static remove = async (req: Request, res: Response) => {
    try {
      const project = await projectRepo.delete(
        req.params.id,
        req.body.tokenData
      );
      if (!project) {
        return res.status(404).json({
          success: false,
        });
      } else {
        return res.status(200).json({
          success: true,
        });
      }
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };
}

export default ProjectController;
