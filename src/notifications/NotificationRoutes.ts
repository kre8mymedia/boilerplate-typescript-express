import express from 'express';
import { authMiddleware } from '../middleware/auth';
import controller from './NotificationController';

const router = express.Router();

router.get('/notifications', authMiddleware, controller.list);
router.post('/notifications', authMiddleware, controller.create);
router.get('/notifications/:id', authMiddleware, controller.find);
router.put('/notifications/:id', authMiddleware, controller.update);
router.patch('/notifications/:id', authMiddleware, controller.update);
router.delete('/notifications/:id', authMiddleware, controller.remove);

export default router;
