import { LeanDocument, Types } from 'mongoose';

// Models
import Notification from './Notification';
// Types
import { TokenData } from '../utils/types';
// Interfaces
import { INotification, NotificationInput } from './NotificationInterface';
// Repositories
import UserRepo from '../users/UserRepo';
import RepositoryInterface from '../interfaces/RepositoryInterface';

const userRepo: UserRepo = new UserRepo();

export default class NotificationRepo
  implements RepositoryInterface<INotification>
{
  public async list(): Promise<INotification[]> {
    try {
      const list = await Notification.find()
        .select(['name', 'hook', 'type', '_id'])
        .exec();
      return list;
    } catch (e) {
      console.error(e);
    }
  }

  public async listByUser(data: TokenData): Promise<
    Omit<
      LeanDocument<
        INotification & {
          _id: Types.ObjectId;
        }
      >,
      never
    >[]
  > {
    try {
      const list = await Notification.find({ user: data._id })
        .lean()
        .select(['name', 'hook', 'type', '_id'])
        .exec();
      return list;
    } catch (e) {
      console.error(e);
    }
  }

  // public async listByOrg(data: TokenData): Promise<INotification[]> {
  //   try {
  //     const list = await Notification.find({ organization: data._id });
  //     return list;
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }

  public async findById(id: string): Promise<INotification> {
    try {
      const model = await Notification.findOne({ _id: id })
        .populate('event', [
          '-notification',
          '-user',
          '-createdAt',
          '-updatedAt',
        ])
        .exec();
      return model;
    } catch (e) {
      console.error(e);
    }
  }

  public async create({ ...input }: NotificationInput): Promise<INotification> {
    const authUser = await userRepo.findById(input.tokenData._id);

    const model = new Notification({
      _id: new Types.ObjectId(),
      ...input,
      organization: authUser.organization,
      user: authUser._id,
    });
    const result = await model.save();
    return result;
  }

  public async update(
    id: string,
    data: NotificationInput
  ): Promise<INotification | null> {
    try {
      // Check if exists
      const notification = await Notification.findById(id);
      if (!notification) return null;

      // Check if authUser
      const authUser = await userRepo.findById(data.tokenData._id);
      if (notification.user._id.toString() !== authUser._id.toString()) {
        return null;
      }

      // Update resource
      await Notification.findByIdAndUpdate(id, data);
      const result = await Notification.findById(id);
      // Return result
      if (!result) return null;
      return result;
    } catch (e) {
      throw new Error(e);
    }
  }

  public async delete(
    id: string,
    data: { _id: string; email?: string }
  ): Promise<boolean> {
    const item = await Notification.findById(id);
    if (!item) return null;
    const authUser = await userRepo.findById(data._id);

    if (item.user._id.toString() !== authUser._id.toString()) {
      return null;
    }

    try {
      await item.delete();
      return true;
    } catch (err) {
      console.error(err);
      return false;
    }
  }
}
