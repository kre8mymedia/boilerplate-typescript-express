import { Request, Response } from 'express';

import NotificationRepo from './NotificationRepo';

const notificationRepo: NotificationRepo = new NotificationRepo();

class NotificationController {
  public static list = async (req: Request, res: Response) => {
    try {
      const notifications = await notificationRepo.listByUser(
        req.body.tokenData
      );
      return res.status(200).json({
        success: true,
        notifications,
      });
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static create = async (req: Request, res: Response) => {
    const { name, hook, type, event, tokenData } = req.body;

    const notification = await notificationRepo.create({
      name,
      hook,
      type,
      event,
      tokenData: tokenData,
    });

    try {
      const result = await notification.save();
      return res.status(201).json({
        success: true,
        notification: result,
      });
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static find = async (req: Request, res: Response) => {
    try {
      const notification = await notificationRepo.findById(req.params.id);
      if (!notification) {
        return res.status(404).json({
          success: false,
        });
      } else {
        return res.status(200).json({
          success: true,
          notification: notification,
        });
      }
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static update = async (req: Request, res: Response) => {
    try {
      const notification = await notificationRepo.update(
        req.params.id,
        req.body
      );

      if (!notification) {
        return res.status(404).json({
          success: false,
        });
      } else {
        return res.status(200).json({
          success: true,
          notification: notification,
        });
      }
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static remove = async (req: Request, res: Response) => {
    try {
      const notification = await notificationRepo.delete(
        req.params.id,
        req.body.tokenData
      );
      if (!notification) {
        return res.status(404).json({
          success: false,
        });
      } else {
        return res.status(200).json({
          success: true,
        });
      }
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };
}

export default NotificationController;
