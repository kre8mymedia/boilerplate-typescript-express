import { Document, Types } from 'mongoose';

// Types
import { TokenData } from '../utils/types';

export interface INotification extends Document {
  name: string;
  hook: string;
  type: string;
  organization: Types.ObjectId;
  user: Types.ObjectId;
  event: Types.ObjectId;
}

export type NotificationInput = {
  name: string;
  hook: string;
  type: string;
  event?: Types.ObjectId;
  tokenData?: TokenData;
};
