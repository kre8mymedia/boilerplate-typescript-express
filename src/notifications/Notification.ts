import { INotification } from './NotificationInterface';
import mongoose, { Schema } from 'mongoose';

const NotificationSchema: Schema = new Schema(
  {
    name: { type: String, required: true },
    type: { type: String, default: 'slack' },
    hook: { type: String, required: true },
    organization: { type: Schema.Types.ObjectId, ref: 'Organization' },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    event: { type: Schema.Types.ObjectId, ref: 'Event' },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: false },
    id: false,
  }
);

NotificationSchema.set('versionKey', false);
NotificationSchema.index({ name: 1, user: 1, event: 1 }, { unique: true });
// NotificationSchema.pre('find', function (next) {
//   this.populate('event');
//   next();
// });

export default mongoose.model<INotification>(
  'Notification',
  NotificationSchema
);
