import express from 'express';
import paymentRoutes from '../payments/PaymentRoutes';

const router = express.Router();

router.use([paymentRoutes]);

export default router;
