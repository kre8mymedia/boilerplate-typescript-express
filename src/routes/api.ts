import express from 'express';
import bookRoutes from '../books/BookRoutes';
import userRoutes from '../users/UserRoutes';
import organizationRoutes from '../organizations/OrganizationRoutes';
import authRoutes from '../auth/AuthRoutes';
import eventRoutes from '../events/EventRoutes';
import projectRoutes from '../projects/ProjectRoutes';
import notificationRoutes from '../notifications/NotificationRoutes';

const router = express.Router();

router.use([
  bookRoutes,
  userRoutes,
  organizationRoutes,
  authRoutes,
  eventRoutes,
  projectRoutes,
  notificationRoutes,
]);

export default router;
