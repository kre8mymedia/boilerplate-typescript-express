import { Express, Request, Response } from 'express';
import swaggerJsdoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';

import { NODE_ENV } from '../config/config';

const servers = [];
if (NODE_ENV === 'test') {
  servers.push({
    url: 'http://localhost:8000/api/v1',
    description: 'Local server',
  });
} else if (NODE_ENV === 'development') {
  servers.push({
    url: 'https://ts-dev-api.glootie.ml/api/v1',
    description: 'Development server',
  });
  servers.push({
    url: 'http://localhost:8000/api/v1',
    description: 'Local server',
  });
} else if (NODE_ENV === 'production') {
  servers.push({
    url: 'https://ts-prod-api.glootie.ml/api/v1',
    description: 'Production server',
  });
}

const options: swaggerJsdoc.Options = {
  swaggerDefinition: {
    openapi: '3.0.1',
    info: {
      title: 'Typescript Express Mongo API',
      version: '1.0.0',
    },
    servers: servers,
    basePath: '/',
    components: {
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT',
        },
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  },
  apis: [
    './src/auth/*.ts',
    './src/users/*.ts',
    './dist/auth/*.js',
    './dist/users/*.js',
    './src/organizations/*.ts',
    './dist/organizations/*.js',
    './src/events/*.ts',
    './dist/events/*.js',
    './src/projects/*.ts',
    './dist/projects/*.js',
  ],
  components: {
    securitySchemas: {
      bearerAuth: {
        type: 'http',
        scheme: 'bearer',
        bearerFormat: 'JWT',
      },
    },
  },
  security: [
    {
      bearerAuth: [],
    },
  ],
};

const swaggerSpec = swaggerJsdoc(options);

function swaggerDocs(app: Express) {
  // Swagger page
  app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

  // Docs in JSON format
  app.get('/docs.json', (req: Request, res: Response) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
  });

  //   log.info(`Docs available at http://localhost:${port}/docs`);
}

export default swaggerDocs;
