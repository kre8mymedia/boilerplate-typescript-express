import bcrypt, { genSaltSync } from 'bcrypt';
import jwt from 'jsonwebtoken';
import axios from 'axios';
import IUser from '../users/UserInterface';
import { APP_SECRET, HOST } from '../config/config';

export const passwordHash = (plainPassword: string): string => {
  const hash = bcrypt.hashSync(plainPassword, genSaltSync(10));
  return hash;
};

export const comparePassword = (
  plainPassword: string,
  passwordHash: string
): boolean => {
  const compared = bcrypt.compareSync(plainPassword, passwordHash);
  return compared;
};

export const generateAuthToken = (user: IUser): string => {
  const token = jwt.sign({ _id: user._id, email: user.email }, APP_SECRET, {
    expiresIn: '12h',
  });

  return token;
};

export const verifyToken = (token: string): { _id: string; email: string } => {
  try {
    const tokenData = jwt.verify(token, APP_SECRET);
    return tokenData as { _id: string; email: string };
  } catch (error) {
    throw new Error(error);
  }
};

export async function fetchTestHeaders() {
  const res = await axios.post(`${HOST}/api/v1/login`, {
    email: 'tester@gmail.com',
    password: 'test1234',
  });
  const token = res.data.token;
  const headers = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  return headers;
}
