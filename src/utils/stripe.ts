import Stripe from 'stripe';
import stripeConfig from '../config/stripe';

export const stripe = new Stripe(stripeConfig.env.secretKey, {
  apiVersion: '2022-08-01',
});

export const getToken = async (code: string): Promise<Stripe.OAuthToken> => {
  try {
    const token = await stripe.oauth.token({
      grant_type: 'authorization_code',
      code,
    });
    return token;
  } catch (error) {
    throw new Error(error.message);
  }
};

export const getAccount = async (connectedAccountId: string) => {
  let account = {};
  try {
    account = await stripe.accounts.retrieve(connectedAccountId);
  } catch (error) {
    throw new Error(error.message);
  }
  return account;
};
