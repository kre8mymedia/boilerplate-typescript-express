export type TokenData = {
  _id: string;
  email?: string;
};
