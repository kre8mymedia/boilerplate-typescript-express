import Book from '../books/Book';
import { IBook, BookInput } from '../books/BookInterface';
import Repository from '../interfaces/RepositoryInterface';
import { Types } from 'mongoose';

import Broker from '../services/broker';

const bookPrefix = 'write/books';

export default class BookRepo implements Repository<IBook> {
  public async list(): Promise<IBook[]> {
    try {
      const books = await Book.find().exec();
      return books;
    } catch (e) {
      console.log(e);
    }
  }

  public async findById(id: string): Promise<IBook> {
    try {
      const book = await Book.findById(id).exec();
      return book;
    } catch (e) {
      console.log(e);
    }
  }

  public async create({ title, author }: BookInput): Promise<IBook> {
    const book = new Book({
      _id: new Types.ObjectId(),
      author,
      title,
    });
    const result = await book.save();
    return result;
  }

  public createFromService({ title, author }: BookInput): boolean {
    const broker = new Broker();
    const result = broker.publish(
      bookPrefix,
      JSON.stringify({ title, author })
    );
    return result;
  }

  public async update(
    id: string,
    { title, author }: BookInput
  ): Promise<IBook> {
    const book = await this.findById(id);
    book.author = author ? author : null;
    book.title = title ? title : null;
    const result = await book.save();
    return result;
  }

  public async delete(id: string): Promise<boolean> {
    try {
      const book = await Book.findByIdAndDelete(id).exec();
      if (book) {
        return true;
      }
    } catch (err) {
      console.log(err);
      return false;
    }
  }
}
