import { Request, Response } from 'express';
import BookRepo from './BookRepo';

const bookRepo = new BookRepo();

class BookController {
  public static list = async (req: Request, res: Response) => {
    console.log(req.session);
    const books = await bookRepo.list();
    return res.status(200).json({
      books: books,
    });
  };

  public static create = async (req: Request, res: Response) => {
    const { author, title } = req.body;

    try {
      const result = await bookRepo.create({ author, title });
      return res.status(201).json({
        book: result,
      });
    } catch (err) {
      return res.status(500).json({
        message: err,
        err,
      });
    }
  };

  public static createFromService = async (req: Request, res: Response) => {
    const { author, title } = req.body;

    try {
      const result = await bookRepo.createFromService({ author, title });
      return res.status(201).json({
        success: result,
      });
    } catch (err) {
      return res.status(500).json({
        message: err,
        err,
      });
    }
  };

  public static find = async (req: Request, res: Response) => {
    try {
      const book = await bookRepo.findById(req.params.id);
      if (!book) {
        return res.status(404).json({
          success: false,
        });
      } else {
        return res.status(200).json({
          book: book,
        });
      }
    } catch (err) {
      return res.status(500).json({
        message: err,
        err,
      });
    }
  };

  public static update = async (req: Request, res: Response) => {
    const { author, title } = req.body;

    try {
      const book = await bookRepo.update(req.params.id, { author, title });

      if (!book) {
        return res.status(404).json({
          success: false,
        });
      } else {
        return res.status(200).json({
          book: book,
        });
      }
    } catch (err) {
      return res.status(500).json({
        message: err,
        err,
      });
    }
  };

  public static remove = async (req: Request, res: Response) => {
    try {
      const book = await bookRepo.delete(req.params.id);
      if (!book) {
        return res.status(404).json({
          success: false,
        });
      } else {
        return res.status(200).json({
          success: true,
        });
      }
    } catch (err) {
      return res.status(500).json({
        message: err,
        err,
      });
    }
  };
}

export default BookController;
