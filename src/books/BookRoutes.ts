import express from 'express';
import controller from './BookController';

const router = express.Router();

router.get('/books', controller.list);
router.post('/books', controller.create);
router.post('/books/pub', controller.createFromService);
router.get('/books/:id', controller.find);
router.put('/books/:id', controller.update);
router.patch('/books/:id', controller.update);
router.delete('/books/:id', controller.remove);

export default router;
