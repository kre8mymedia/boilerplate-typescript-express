import { IBook } from './BookInterface';
import mongoose, { Schema } from 'mongoose';

const BookSchema: Schema = new Schema(
  {
    title: { type: String, required: true },
    author: { type: String, required: true },
    extraInformation: { type: String },
  },
  {
    timestamps: true,
    toObject: { versionKey: false },
  }
);

// BookSchema.post<IBook>('save', () => {
//   this.extraInformation = 'This is some extra info we want to push onto this entry after save';
// });

export default mongoose.model<IBook>('Book', BookSchema);
