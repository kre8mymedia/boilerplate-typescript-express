import { Document } from 'mongoose';

export interface IBook extends Document {
  title: string;
  author: string;
  extraInformation?: string;
}

export type BookInput = {
  title: string;
  author: string;
};
