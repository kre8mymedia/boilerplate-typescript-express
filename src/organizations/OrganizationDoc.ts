/**
 * --------------------------------------------------------------------------------------
 * Organization Schema
 * --------------------------------------------------------------------------------------
 * @openapi
 * components:
 *   schemas:
 *     Organization:
 *       type: object
 *       properties:
 *         _id:
 *           type: uid
 *           description: The organization ID.
 *           example: 62f94d2d0890a74abca873ca
 *         name:
 *           type: string
 *           description: Name of the organization
 *           example: Capsule Corp
 *         slug:
 *           type: string
 *           description: Unique slug for url param readable id
 *           example: capsule-corp
 *         createdAt:
 *           type: timestamp
 *           description: When created
 *           example: 2022-05-16T16:00:00.000Z
 */

/**
 * --------------------------------------------------------------------------------------
 * Organization Schema
 * --------------------------------------------------------------------------------------
 * @openapi
 * components:
 *   schemas:
 *     OrganizationWithPrimaryUser:
 *       type: object
 *       properties:
 *         _id:
 *           type: uid
 *           description: The organization ID.
 *           example: 62f94d2d0890a74abca873ca
 *         name:
 *           type: string
 *           description: Name of the organization
 *           example: Capsule Corp
 *         slug:
 *           type: string
 *           description: Unique slug for url param readable id
 *           example: capsule-corp
 *         primaryUser:
 *           type: object
 *           $ref: '#/components/schemas/User'
 */

/**
 * --------------------------------------------------------------------------------------
 * List organizations
 * --------------------------------------------------------------------------------------
 * @openapi
 * /organizations/all:
 *   get:
 *     security: []
 *     tags:
 *       - Organizations
 *     summary: List Organizations
 *     description: List all organizations that exist
 *     responses:
 *       200:
 *         description: A list of organizations.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  success:
 *                      type: boolean
 *                      example: true
 *                  organizations:
 *                      items:
 *                        $ref: '#/components/schemas/Organization'
 */

/**
 * --------------------------------------------------------------------------------------
 * Create Organization
 * --------------------------------------------------------------------------------------
 *
 * @openapi
 * /organizations:
 *   post:
 *     security:
 *      - bearerAuth: []
 *     tags:
 *       - Organizations
 *     summary: Create Organization
 *     description: creates a new organization
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: Capsule Corp
 *               slug:
 *                 type: string
 *                 example: capsule-corp
 *     responses:
 *       201:
 *         description: Creates organization
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  success:
 *                      type: boolean
 *                      example: true
 *                  organization:
 *                      type: object
 *                      $ref: '#/components/schemas/Organization'
 *       422:
 *         description: Validation error
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/ValidationError'
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/NotFoundResponse'
 */

/**
 * --------------------------------------------------------------------------------------
 * Fetch Organization by ID
 * --------------------------------------------------------------------------------------
 * @openapi
 * /organizations:
 *   get:
 *     tags:
 *       - Organizations
 *     summary: Retreive Organization
 *     description: retrieve organization
 *     responses:
 *       200:
 *         description: A single organization.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  success:
 *                      type: boolean
 *                      example: true
 *                  organization:
 *                      type: object
 *                      $ref: '#/components/schemas/OrganizationWithPrimaryUser'
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/NotFoundResponse'
 */

/**
 * --------------------------------------------------------------------------------------
 * Update Organization by ID
 * --------------------------------------------------------------------------------------
 * @openapi
 * /organizations/{id}:
 *   put:
 *     tags:
 *       - Organizations
 *     summary: Update Organization by ID
 *     description: retrieve organization by id
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: ID of the organization to fetch resource
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: Capsule Corp
 *               slug:
 *                 type: string
 *                 example: capsule-corp
 *     responses:
 *       200:
 *         description: Update single organization.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  success:
 *                      type: boolean
 *                      example: true
 *                  organization:
 *                      type: object
 *                      $ref: '#/components/schemas/OrganizationWithPrimaryUser'
 *       422:
 *         description: Validation error
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/ValidationError'
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/NotFoundResponse'
 */

/**
 * --------------------------------------------------------------------------------------
 * Delete Organization by ID
 * --------------------------------------------------------------------------------------
 * @openapi
 * /organizations/{id}:
 *   delete:
 *     tags:
 *       - Organizations
 *     summary: Delete Organization by ID
 *     description: Delete organization by id
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: ID of the organization to fetch resource
 *     responses:
 *       200:
 *         description: Delete single organization.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  success:
 *                      type: boolean
 *                      example: true
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/NotFoundResponse'
 */
