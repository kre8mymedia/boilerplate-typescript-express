import { IOrganization } from './OrganizationInterface';
import mongoose, { Schema } from 'mongoose';

const OrganizationSchema: Schema = new Schema(
  {
    name: { type: String, required: true },
    slug: { type: String, required: true, unique: true },
    primaryUser: { type: Schema.Types.ObjectId, ref: 'User' },
  },
  {
    timestamps: true,
    toObject: { versionKey: false },
  }
);

OrganizationSchema.set('versionKey', false);
OrganizationSchema.index({ primaryUser: 1 }, { unique: true });

export default mongoose.model<IOrganization>(
  'Organization',
  OrganizationSchema
);
