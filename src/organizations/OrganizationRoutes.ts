import express from 'express';
import { authMiddleware } from '../middleware/auth';
import controller from '../organizations/OrganizationController';

const router = express.Router();

router.get('/organizations/all', controller.list);

// Auth routes
router.post('/organizations', authMiddleware, controller.create);
router.get('/organizations', authMiddleware, controller.find);
router.put('/organizations/:id', authMiddleware, controller.update);
router.patch('/organizations/:id', authMiddleware, controller.update);
router.delete('/organizations/:id', authMiddleware, controller.remove);

export default router;
