import { Request, Response } from 'express';
import Organization from './Organization';
import User from '../users/User';

import OrganizationRepo from './OrganizationRepo';

const organizationRepo: OrganizationRepo = new OrganizationRepo();

class OrganizationController {
  public static list = async (req: Request, res: Response) => {
    try {
      const organizations = await organizationRepo.list();
      return res.status(200).json({
        success: true,
        organizations,
      });
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static create = async (req: Request, res: Response) => {
    const { name, slug, tokenData } = req.body;

    const exist = await Organization.findOne({ primaryUser: tokenData._id });
    if (exist) {
      return res.status(400).json({
        success: false,
        message: 'Duplicate Entry',
      });
    }

    const organization = await organizationRepo.create({
      name,
      slug,
      primaryUser: tokenData._id,
    });

    const authUser = await User.findById(tokenData._id);
    authUser.organization = organization._id;
    await authUser.save();

    try {
      const result = await (
        await organization.populate('primaryUser', [
          '-password',
          '-organization',
          '-createdAt',
          '-updatedAt',
        ])
      ).save();
      return res.status(201).json({
        success: true,
        organization: result,
      });
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static find = async (req: Request, res: Response) => {
    try {
      const organization = await organizationRepo.findByUser(
        req.body.tokenData
      );
      if (!organization) {
        return res.status(404).json({
          success: false,
        });
      } else {
        return res.status(200).json({
          success: true,
          organization: organization,
        });
      }
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static update = async (req: Request, res: Response) => {
    try {
      const organization = await organizationRepo.update(
        req.params.id,
        req.body
      );

      if (!organization) {
        return res.status(404).json({
          success: false,
        });
      } else {
        return res.status(200).json({
          success: true,
          organization: organization,
        });
      }
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static remove = async (req: Request, res: Response) => {
    try {
      const organization = await organizationRepo.delete(
        req.params.id,
        req.body.tokenData
      );
      if (!organization) {
        return res.status(404).json({
          success: false,
        });
      } else {
        return res.status(200).json({
          success: true,
        });
      }
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };
}

export default OrganizationController;
