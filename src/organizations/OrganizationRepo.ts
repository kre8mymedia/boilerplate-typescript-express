import Organization from './Organization';
import {
  IOrganization,
  OrganizationInput,
} from '../organizations/OrganizationInterface';
import Repository from '../interfaces/RepositoryInterface';
import { Types } from 'mongoose';
import UserRepo from '../users/UserRepo';

const userRepo: UserRepo = new UserRepo();

export default class OrganizationRepo implements Repository<IOrganization> {
  public async list(): Promise<IOrganization[]> {
    try {
      const list = await Organization.find()
        .select(['name', 'slug', '_id', 'createdAt'])
        .exec();
      return list;
    } catch (e) {
      console.log(e);
    }
  }

  public async findById(id: string): Promise<IOrganization> {
    try {
      const model = await (
        await Organization.findById(id)
      ).populate('primaryUser', [
        '-password',
        '-createdAt',
        '-updatedAt',
        '-organization',
      ]);
      return model;
    } catch (e) {
      console.log(e);
    }
  }

  public async findByUser(tokenData: {
    _id: string;
    email: string;
  }): Promise<IOrganization> {
    const authUser = await userRepo.findById(tokenData._id);
    try {
      const model = await (
        await Organization.findById(authUser.organization)
      ).populate('primaryUser', [
        '-password',
        '-createdAt',
        '-updatedAt',
        '-organization',
      ]);
      return model;
    } catch (e) {
      console.log(e);
    }
  }

  public async create({ ...input }: OrganizationInput): Promise<IOrganization> {
    const model = new Organization({
      _id: new Types.ObjectId(),
      ...input,
    });
    const result = await model.save();
    return result;
  }

  public async update(
    id: string,
    data: OrganizationInput
  ): Promise<IOrganization | null> {
    const item = await this.findById(id);
    if (!item) return null;
    const authUser = await userRepo.findById(data.tokenData._id);
    if (item.primaryUser._id.toString() !== authUser._id.toString()) {
      return null;
    }

    item.name = data.name ? data.name : null;
    const result = await item.save();
    return result;
  }

  public async delete(
    id: string,
    data: { _id: string; email?: string }
  ): Promise<boolean> {
    const item = await this.findById(id);
    if (!item) return null;
    const authUser = await userRepo.findById(data._id);

    if (item.primaryUser._id.toString() !== authUser._id.toString()) {
      return null;
    }

    try {
      const model = await item.delete();
      if (model) {
        authUser.organization = null;
        authUser.save();
        return true;
      }
    } catch (err) {
      console.log(err);
      return false;
    }
  }
}
