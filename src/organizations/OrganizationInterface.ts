import { Document, Types } from 'mongoose';

export interface IOrganization extends Document {
  name: string;
  slug: string;
  primaryUser: Types.ObjectId;
}

export type OrganizationInput = {
  name: string;
  slug?: string;
  primaryUser?: Types.ObjectId;
  tokenData?: { _id: string; email?: string };
};
