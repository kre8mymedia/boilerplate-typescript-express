import { Types } from 'mongoose';
import User from '../users/User';
import IUser, {
  LoginInput,
  RegisterInput,
  TokenData,
} from '../users/UserInterface';
import { comparePassword, generateAuthToken } from '../utils/auth';
import SendInBlueService, {
  REGISTERED_USER_LIST_ID,
} from '../services/SendInBlueService';
import { SEND_IN_BLUE_API_KEY } from '../config/config';

export default class AuthRepo {
  public async register({ ...input }: RegisterInput): Promise<IUser> {
    try {
      const user = new User({
        _id: new Types.ObjectId(),
        ...input,
      });
      const result = await user.save();
      // Register
      const addContact = new SendInBlueService(SEND_IN_BLUE_API_KEY);
      await addContact.createContact({
        email: input.email,
        updateEnabled: true,
        listIds: [REGISTERED_USER_LIST_ID],
      });
      // console.log('registerUser: ', contact);
      return result;
    } catch (e) {
      throw new Error(e.message);
    }
  }

  public async authUser({ ...tokenData }: TokenData): Promise<IUser> {
    try {
      const user = await User.findById(tokenData._id)
        .select([
          '_id',
          'firstname',
          'lastname',
          'email',
          'username',
          'updatedAt',
        ])
        .exec();
      return user;
    } catch (e) {
      throw new Error(e.message);
    }
  }

  public async login({
    email,
    password,
  }: LoginInput): Promise<string | boolean> {
    try {
      const user = await User.findOne({ email });
      if (!user) {
        return false;
      }
      const compare = comparePassword(password, user.password);
      if (compare) {
        const token = generateAuthToken(user);
        return token;
      }
    } catch (e) {
      throw new Error(e.message);
    }
  }
}
