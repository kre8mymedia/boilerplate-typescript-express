import express from 'express';
import { authMiddleware } from '../middleware/auth';
import controller from './AuthController';

const router = express.Router();

router.post('/register', controller.register);
router.post('/login', controller.login);
router.get('/auth', authMiddleware, controller.authUser);

export default router;
