import { Request, Response } from 'express';

import AuthRepo from './AuthRepo';

const authRepo = new AuthRepo();

class AuthController {
  public static register = async (req: Request, res: Response) => {
    try {
      const result = await authRepo.register(req.body);
      if (!result) {
        return res.status(500).json({
          success: false,
        });
      } else {
        return res.status(201).json({
          success: true,
        });
      }
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static login = async (req: Request, res: Response) => {
    try {
      const token = await authRepo.login(req.body);
      if (token) {
        return res.status(200).json({
          success: true,
          token,
        });
      } else {
        return res.status(401).json({
          success: false,
          message: 'Unauthorized',
        });
      }
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static authUser = async (req: Request, res: Response) => {
    try {
      const authUser = await authRepo.authUser(req.body.tokenData);
      if (!authUser) {
        return res.status(404).json({
          success: false,
        });
      } else {
        return res.status(200).json({
          success: true,
          authUser,
        });
      }
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };
}

export default AuthController;
