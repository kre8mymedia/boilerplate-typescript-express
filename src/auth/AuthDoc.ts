/**
 * --------------------------------------------------------------------------------------
 * Success Response
 * --------------------------------------------------------------------------------------
 * @openapi
 * components:
 *   schemas:
 *     SuccessResponse:
 *       type: object
 *       properties:
 *         success:
 *           type: string
 *           description: Success response
 *           example: true
 */

/**
 * --------------------------------------------------------------------------------------
 * Success Response With Token
 * --------------------------------------------------------------------------------------
 * @openapi
 * components:
 *   schemas:
 *     SuccessResponseWithToken:
 *       type: object
 *       properties:
 *         success:
 *           type: string
 *           description: Success message
 *           example: true
 *         token:
 *           type: string
 *           description: JWT Token
 *           example: eyJhbGciOiJIUzI1N...
 */

/**
 * --------------------------------------------------------------------------------------
 * Error Response
 * --------------------------------------------------------------------------------------
 * @openapi
 * components:
 *   schemas:
 *     ErrorResponse:
 *       type: object
 *       properties:
 *         success:
 *           type: string
 *           description: Error response
 *           example: false
 */

/**
 * --------------------------------------------------------------------------------------
 * Not Found Response
 * --------------------------------------------------------------------------------------
 * @openapi
 * components:
 *   schemas:
 *     NotFoundResponse:
 *       type: object
 *       properties:
 *         success:
 *           type: string
 *           example: false
 *         message:
 *           type: string
 *           example: No resource found
 */

/**
 * --------------------------------------------------------------------------------------
 * Error Response With Message
 * --------------------------------------------------------------------------------------
 * @openapi
 * components:
 *   schemas:
 *     ErrorResponseWithMessage:
 *       type: object
 *       properties:
 *         success:
 *           type: string
 *           description: Error message
 *           example: false
 *         message:
 *           type: string
 *           description: Error response message
 *           example: "Erorr doing some task..."
 */

/**
 * --------------------------------------------------------------------------------------
 * Error Response With Message
 * --------------------------------------------------------------------------------------
 * @openapi
 * components:
 *   schemas:
 *     ValidationError:
 *       type: object
 *       properties:
 *         success:
 *           type: string
 *           description: Error message
 *           example: false
 *         message:
 *           type: string
 *           description: Error response message
 *           example: "Something is wrong with one of your fields"
 */

/**
 * --------------------------------------------------------------------------------------
 * Error Response With Message
 * --------------------------------------------------------------------------------------
 * @openapi
 * components:
 *   schemas:
 *     Unauthorized:
 *       type: object
 *       properties:
 *         success:
 *           type: string
 *           description: Successful or not..
 *           example: false
 *         message:
 *           type: string
 *           description: Unauthorized message
 *           example: "Unauthorized"
 */

/**
 * --------------------------------------------------------------------------------------
 * Register User
 * --------------------------------------------------------------------------------------
 * @openapi
 * /register:
 *   post:
 *     security: []
 *     tags:
 *       - Auth
 *     summary: Register user
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               firstname:
 *                 type: string
 *                 description: The user's first name.
 *                 example: Glootius
 *               lastname:
 *                 type: string
 *                 description: The user's last name.
 *                 example: Maximus
 *               username:
 *                 type: string
 *                 description: Unique Username
 *                 example: glootie
 *               email:
 *                 type: string
 *                 description: User email
 *                 example: glootie@gmail.com
 *               password:
 *                 type: string
 *                 description: Passed
 *                 example: test1234
 *     description: Register as user of application
 *     responses:
 *       200:
 *         description: Success response
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/SuccessResponse'
 *       400:
 *         description: Validation error
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/ValidationError'
 */

/**
 * --------------------------------------------------------------------------------------
 * Login
 * --------------------------------------------------------------------------------------
 * @openapi
 * /login:
 *   post:
 *     security: []
 *     tags:
 *       - Auth
 *     summary: Retrieve login token
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *                 example: glootie@gmail.com
 *               password:
 *                 type: string
 *                 example: test1234
 *     description: Login user to retrieve token
 *     responses:
 *       200:
 *         description: Success response
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/SuccessResponseWithToken'
 *       401:
 *         description: Unauthorized
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/Unauthorized'
 */
