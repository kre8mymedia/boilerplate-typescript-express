export default interface Repository<T> {
  list(): Promise<T[]>;
  findById(id: string): Promise<T>;
  create(item: T): Promise<T>;
  update(id: string, item: T): Promise<T>;
  delete(id: string, data?: { _id: string; email: string }): Promise<boolean>;
}
