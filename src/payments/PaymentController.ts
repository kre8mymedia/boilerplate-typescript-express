import { Request, Response } from 'express';
import Stripe from 'stripe';
import stripeConfig from '../config/stripe';
import { getToken, getAccount, stripe } from '../utils/stripe';

class PaymentController {
  public static auth = (req: Request, res: Response) => {
    const queryData = {
      response_type: 'code',
      client_id: stripeConfig.env.clientId,
      scope: 'read_write',
      redirect_uri: stripeConfig.env.redirectUri,
    };
    const connectUri = `${stripeConfig.env.authorizationUri}?response_type=${queryData.response_type}&client_id=${queryData.client_id}&scope=${queryData.scope}&redirect_uri=${queryData.redirect_uri}`;
    res.status(301).redirect(connectUri);
  };

  public static redirect = async (req: Request, res: Response) => {
    try {
      const token: Stripe.OAuthToken = await getToken(
        req.query.code.toString()
      );
      console.log('PaymentsController.redirect: ', token);
      const connectedAccountId = token.stripe_user_id;
      const account = await getAccount(connectedAccountId);
      const link = await stripe.accounts.createLoginLink(connectedAccountId);

      return res.status(200).json({ account, link });
    } catch (e) {
      return res.status(400).json({
        success: false,
        message: e.message,
      });
    }
  };
}

export default PaymentController;
