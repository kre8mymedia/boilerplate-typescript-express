import express from 'express';
import controller from './PaymentController';

const router = express.Router();

router.get('/auth', controller.auth);
router.get('/redirect', controller.redirect);

export default router;
