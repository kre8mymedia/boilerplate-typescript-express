import { Document, Types } from 'mongoose';
// Types
import { TokenData } from '../utils/types';
import { INotification } from '../notifications/NotificationInterface';

export interface IEvent extends Document {
  title: string;
  start: number;
  end?: number;
  location?: string;
  description?: string;
  bgColor: string;
  hours: number;
  user: Types.ObjectId;
  project?: Types.ObjectId;
  notifications: INotification[];
}

export type EventInput = {
  title: string;
  start?: number;
  end?: number;
  bgColor?: string;
  location?: string;
  description?: string;
  user?: Types.ObjectId;
  project?: Types.ObjectId;
  notifications?: INotification[];
  tokenData?: TokenData;
};
