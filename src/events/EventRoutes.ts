import express from 'express';
import { authMiddleware } from '../middleware/auth';
import controller from '../events/EventController';

const router = express.Router();

// ONLY ACCESSIBLE FROM SCRAPER USER
router.get('/events/all', authMiddleware, controller.all);

router.get('/events', authMiddleware, controller.list);
router.post('/events', authMiddleware, controller.create);
router.get('/events/:id', authMiddleware, controller.find);
router.put('/events/:id', authMiddleware, controller.update);
router.patch('/events/:id', authMiddleware, controller.update);
router.delete('/events/:id', authMiddleware, controller.remove);

export default router;
