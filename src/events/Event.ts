import { IEvent } from './EventInterface';
import mongoose, { Schema } from 'mongoose';
import Notification from '../notifications/Notification';

const EventSchema: Schema = new Schema(
  {
    title: { type: String, required: true },
    start: { type: Date, required: true, default: Date.now },
    bgColor: { type: String, default: '#8C8C8C' },
    end: { type: Date, default: Date.now() + 3600 },
    location: { type: String },
    description: { type: String },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    project: { type: Schema.Types.ObjectId, ref: 'Project' },
    notifications: [{ type: Schema.Types.ObjectId, ref: Notification }],
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
    id: false,
  }
);

EventSchema.set('versionKey', false);
EventSchema.virtual('hours').get(function () {
  return (this.end - this.start) / 1000 / 60 / 60;
});
// EventSchema.pre('find', function (next) {
//   this.populate('project', ['-organization']);
//   next();
// });
// EventSchema.pre('find', function (next) {
//   this.populate('notifications');
//   next();
// });

export default mongoose.model<IEvent>('Event', EventSchema);
