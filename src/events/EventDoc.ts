/**
 * --------------------------------------------------------------------------------------
 * Event Schema
 * --------------------------------------------------------------------------------------
 * @openapi
 * components:
 *   schemas:
 *     Event:
 *       type: object
 *       properties:
 *         _id:
 *           type: uid
 *           description: The event ID.
 *           example: 62f94d2d0890a74abca873ca
 *         title:
 *           type: string
 *           description: Name of the event
 *           example: Ski vacation
 *         start:
 *           type: integer
 *           description: Timestamp for event start
 *           example: 1652716800000
 *         end:
 *           type: integer
 *           description: Timestamp for event end
 *           example: 1652724000000
 *         bgColor:
 *           type: string
 *           description: Default event color
 *           example: "light-gray"
 *         hours:
 *           type: integer
 *           description: Difference between start and end of event
 *           example: 2
 *         location:
 *           type: string
 *           description: Location of the event
 *           example: 8302 S Brighton Loop Rd, Brighton, UT 84121
 *         description:
 *           type: string
 *           description: Description of the event
 *           example: Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
 *         createdAt:
 *           type: timestamp
 *           description: When created
 *           example: 2022-05-16T16:00:00.000Z
 */

/**
 * --------------------------------------------------------------------------------------
 * Event Schema
 * --------------------------------------------------------------------------------------
 * @openapi
 * components:
 *   schemas:
 *     EventWithUser:
 *       type: object
 *       properties:
 *         _id:
 *           type: uid
 *           description: The event ID.
 *           example: 62f94d2d0890a74abca873ca
 *         name:
 *           type: string
 *           description: Name of the event
 *           example: Ski vacation
 *         start:
 *           type: integer
 *           description: Timestamp for event start
 *           example: 1652716800000
 *         end:
 *           type: integer
 *           description: Timestamp for event end
 *           example: 1652724000000
 *         location:
 *           type: string
 *           description: Location of the event
 *           example: 8302 S Brighton Loop Rd, Brighton, UT 84121
 *         description:
 *           type: string
 *           description: Description of the event
 *           example: Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
 *
 *         user:
 *           type: object
 *           $ref: '#/components/schemas/User'
 */

/**
 * --------------------------------------------------------------------------------------
 * List events
 * --------------------------------------------------------------------------------------
 * @openapi
 * /events:
 *   get:
 *     tags:
 *       - Events
 *     summary: List Events
 *     description: List all events that exist
 *     responses:
 *       200:
 *         description: A list of events.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  success:
 *                      type: boolean
 *                      example: true
 *                  events:
 *                      items:
 *                        $ref: '#/components/schemas/Event'
 */

/**
 * --------------------------------------------------------------------------------------
 * Create Event
 * --------------------------------------------------------------------------------------
 *
 * @openapi
 * /events:
 *   post:
 *     security:
 *      - bearerAuth: []
 *     tags:
 *       - Events
 *     summary: Create Event
 *     description: creates a new event
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *                 description: Name of the event
 *                 example: Ski vacation
 *               start:
 *                 type: integer
 *                 description: Timestamp for event start
 *                 example: 1652716800000
 *               end:
 *                 type: integer
 *                 description: Timestamp for event end
 *                 example: 1652716800000
 *               bgColor:
 *                 type: string
 *                 description: Default event color
 *                 example: #8C8C8C
 *               location:
 *                 type: string
 *                 description: Location of the event
 *                 example: 8302 S Brighton Loop Rd, Brighton, UT 84121
 *               description:
 *                 type: string
 *                 description: Description of the event
 *                 example: Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
 *     responses:
 *       201:
 *         description: Creates event
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  success:
 *                      type: boolean
 *                      example: true
 *                  event:
 *                      type: object
 *                      $ref: '#/components/schemas/EventWithUser'
 *       422:
 *         description: Validation error
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/ValidationError'
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/NotFoundResponse'
 */

/**
 * --------------------------------------------------------------------------------------
 * Fetch Event by ID
 * --------------------------------------------------------------------------------------
 * @openapi
 * /events/{id}:
 *   get:
 *     tags:
 *       - Events
 *     summary: Retreive Event
 *     description: retrieve event
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: ID of the event to fetch resource
 *     responses:
 *       200:
 *         description: A single event.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  success:
 *                      type: boolean
 *                      example: true
 *                  event:
 *                      type: object
 *                      $ref: '#/components/schemas/EventWithUser'
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/NotFoundResponse'
 */

/**
 * --------------------------------------------------------------------------------------
 * Update Event by ID
 * --------------------------------------------------------------------------------------
 * @openapi
 * /events/{id}:
 *   put:
 *     tags:
 *       - Events
 *     summary: Update Event by ID
 *     description: retrieve event by id
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: ID of the event to fetch resource
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *                 description: Name of the event
 *                 example: Ski vacation
 *               start:
 *                 type: integer
 *                 description: Timestamp for event start
 *                 example: 1652716800000
 *               end:
 *                 type: integer
 *                 description: Timestamp for event end
 *                 example: 1652716800000
 *               bgColor:
 *                 type: string
 *                 description: Default event color
 *                 example: #8C8C8C
 *               location:
 *                 type: string
 *                 description: Location of the event
 *                 example: 8302 S Brighton Loop Rd, Brighton, UT 84121
 *               description:
 *                 type: string
 *                 description: Description of the event
 *                 example: Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
 *     responses:
 *       200:
 *         description: Update single event.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  success:
 *                      type: boolean
 *                      example: true
 *                  event:
 *                      type: object
 *                      $ref: '#/components/schemas/EventWithUser'
 *       422:
 *         description: Validation error
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/ValidationError'
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/NotFoundResponse'
 */

/**
 * --------------------------------------------------------------------------------------
 * Delete Event by ID
 * --------------------------------------------------------------------------------------
 * @openapi
 * /events/{id}:
 *   delete:
 *     tags:
 *       - Events
 *     summary: Delete Event by ID
 *     description: Delete event by id
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: ID of the event to fetch resource
 *     responses:
 *       200:
 *         description: Delete single event.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  success:
 *                      type: boolean
 *                      example: true
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/NotFoundResponse'
 */
