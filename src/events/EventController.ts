import { Request, Response } from 'express';

import { SCRAPER_USER_EMAIL } from '../config/config';

import EventRepo from './EventRepo';
import UserRepo from '../users/UserRepo';
import { IEvent } from './EventInterface';

const eventRepo: EventRepo = new EventRepo();
const userRepo: UserRepo = new UserRepo();

class EventController {
  public static all = async (req: Request, res: Response) => {
    try {
      const authUser = await userRepo.findById(req.body.tokenData._id);
      if (SCRAPER_USER_EMAIL !== authUser.email) {
        return res.status(401).json({
          success: false,
          message: 'Unauthorized',
        });
      }
      const events = await eventRepo.listAllLean();
      return res.status(200).json({
        success: true,
        events,
      });
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static list = async (req: Request, res: Response) => {
    try {
      let events;
      if (req.query.start && req.query.end) {
        events = await eventRepo.listByUser(req.body.tokenData, {
          start: parseInt(req.query.start.toString()),
          end: parseInt(req.query.end.toString())
        });
      } else {
        events = await eventRepo.listByUser(req.body.tokenData);
      }
      return res.status(200).json({
        success: true,
        events,
      });
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static create = async (req: Request, res: Response) => {
    const {
      title,
      start,
      end,
      location,
      description,
      project,
      notifications,
      tokenData,
    } = req.body;

    const event = await eventRepo.create({
      title,
      start,
      end,
      location,
      description,
      project,
      notifications,
      user: tokenData._id,
    });

    try {
      const result = await event.save();
      return res.status(201).json({
        success: true,
        event: result,
      });
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static find = async (req: Request, res: Response) => {
    try {
      const event = await eventRepo.leanFindById(req.params.id);
      if (!event) {
        return res.status(404).json({
          success: false,
        });
      } else {
        return res.status(200).json({
          success: true,
          event: event,
        });
      }
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static update = async (req: Request, res: Response) => {
    try {
      const event = await eventRepo.update(req.params.id, req.body);

      if (!event) {
        return res.status(404).json({
          success: false,
        });
      } else {
        return res.status(200).json({
          success: true,
          event: event,
        });
      }
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };

  public static remove = async (req: Request, res: Response) => {
    try {
      const event = await eventRepo.delete(req.params.id, req.body.tokenData);
      if (!event) {
        return res.status(404).json({
          success: false,
        });
      } else {
        return res.status(200).json({
          success: true,
        });
      }
    } catch (err) {
      return res.status(500).json({
        message: err.message,
        err,
      });
    }
  };
}

export default EventController;
