import { LeanDocument, Types } from 'mongoose';

// Models
import Event from './Event';
// Types
import { TokenData } from '../utils/types';
// Interfaces
import { IEvent, EventInput } from '../events/EventInterface';
// Repositories
import RepositoryInterface from '../interfaces/RepositoryInterface';
import UserRepo from '../users/UserRepo';

const userRepo: UserRepo = new UserRepo();

export default class EventRepo implements RepositoryInterface<IEvent> {
  public async list(): Promise<IEvent[]> {
    try {
      const list = await Event.find().select([
        'title',
        'bgColor',
        'start',
        'end',
        'location',
        'description',
        '_id',
        'createdAt',
      ]);
      return list;
    } catch (e) {
      console.log(e);
    }
  }

  public async listAllLean(): Promise<
    Omit<
      LeanDocument<
        IEvent & {
          _id: Types.ObjectId;
        }
      >,
      never
    >[]
  > {
    try {
      const list = await Event.find()
        .lean()
        .populate('notifications', '-organization -user')
        .select([
          'title',
          'bgColor',
          'start',
          'end',
          'location',
          'description',
          '_id',
          'createdAt',
        ])
        .exec();
      return list;
    } catch (e) {
      console.log(e);
    }
  }

  public async listByUser(
    tokenData: TokenData, 
    queryInfo?: { 
      start: number,
       end: number 
    }
  ): Promise<
    Omit<
      LeanDocument<
        IEvent & {
          _id: Types.ObjectId;
        }
      >,
      never
    >[]
  > {
    let query;
    if (queryInfo) {
      query = { 
        user: tokenData._id,
        start: {
          $gte: queryInfo.start
        }, 
        end: {
          $lte: queryInfo.end
        }
      }
    } else {
      query = { 
        user: tokenData._id,
      }
    }
    try {
      const list = await Event.find(query)
        .lean()
        .populate('project', '-events -organization -user')
        .populate('notifications', ['-organization', '-user'])
        .select([
          'title',
          'bgColor',
          'start',
          'end',
          'location',
          'description',
          '_id',
          'createdAt',
        ])
        .exec();
      return list;
    } catch (e) {
      console.log(e);
    }
  }

  public async findById(id: string): Promise<IEvent> {
    try {
      const model = await Event.findOne({ _id: id })
        .populate('project')
        .populate('notifications', '-event -organization -user')
        .populate('user', [
          '-password',
          '-createdAt',
          '-updatedAt',
          '-organization',
        ]);
      return model;
    } catch (e) {
      console.log(e);
    }
  }

  public async leanFindById(id: string): Promise<
    Omit<
      LeanDocument<
        IEvent & {
          _id: Types.ObjectId;
        }
      >,
      never
    >
  > {
    try {
      const model = await Event.findOne({ _id: id })
        .lean()
        .select(['_id', 'title', 'start', 'end', 'description'])
        .populate('notifications', '-event -organization -user')
        .populate('project', '-events -organization -user')
        .populate('user', [
          '-password',
          '-createdAt',
          '-updatedAt',
          '-organization',
        ])
        .exec();
      return model;
    } catch (e) {
      console.log(e);
    }
  }

  public async create({ ...input }: EventInput) {
    const model = new Event({
      _id: new Types.ObjectId(),
      ...input,
    });

    const result = await model.save();
    return result;
  }

  public async update(id: string, data: EventInput): Promise<IEvent | null> {
    const model = await this.findById(id);
    if (!model) return null;
    const authUser = await userRepo.findById(data.tokenData._id);
    if (model.user._id.toString() !== authUser._id.toString()) {
      return null;
    }

    const params = { ...data };
    if (data.notifications) {
      if (model.notifications) {
        params['notifications'] = [...data.notifications];
      } else {
        const notifications = model.notifications != null ? [...model.notifications] : [];
        params['notifications'] = [...notifications, ...data.notifications];
      }
    }
    await Event.findByIdAndUpdate(id, params);
    const result = await this.findById(id);
    if (!result) return null;
    return result;
  }

  public async delete(
    id: string,
    data: { _id: string; email?: string }
  ): Promise<boolean> {
    const item = await this.findById(id);
    if (!item) return null;
    const authUser = await userRepo.findById(data._id);

    if (item.user._id.toString() !== authUser._id.toString()) {
      return null;
    }

    try {
      await item.delete();
      return true;
    } catch (err) {
      console.log(err);
      return false;
    }
  }
}
