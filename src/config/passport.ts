import passport from 'passport';
import google from 'passport-google-oauth20';
import IUser from '../users/UserInterface';
import User from '../users/User';
// import { generateAuthToken } from '../utils/auth';
import { GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET, HOST } from './config';

const GoogleStrategy = google.Strategy;

passport.use(
  new GoogleStrategy(
    {
      clientID: GOOGLE_CLIENT_ID,
      clientSecret: GOOGLE_CLIENT_SECRET,
      callbackURL: `${HOST}/auth/google/callback`,
      passReqToCallback: true,
    },
    function (request, accessToken, refreshToken, profile, done) {
      console.log('New GoogleLogin', profile);

      User.findOne({ googleId: profile.id }, (err: Error, user: IUser) => {
        if (err) {
          return done(err);
        }
        if (!user) {
          const userName = profile.emails[0].value.split('@');
          user = new User({
            username: userName[0],
            firstname: profile.name.givenName,
            lastname: profile.name.familyName,
            email: profile.emails[0].value,
            googleId: profile.id,
          });
          user.save((err: Error) => {
            if (err) console.log(err);
            return done(err, user);
          });
        } else {
          //found user. Return
          return done(err, user);
        }
      });
    }
  )
);

passport.serializeUser(function (user: IUser, done) {
  done(null, user._id);
});

passport.deserializeUser(function (id: string, done) {
  User.findById(id, (err: Error, doc: IUser) => {
    return done(null, doc);
  });
});
