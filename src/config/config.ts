import 'dotenv/config';

const MONGO_USERNAME = process.env.MONGO_USERNAME || 'testuser';
const MONGO_PASSWORD = process.env.MONGO_PASSWORD || 'test1234';
const MONGO_HOST = process.env.MONGO_HOST || 'localhost';
export const NODE_ENV = process.env.NODE_ENV || 'remote';

export const APP_SECRET = process.env.APP_SECRET || 'testKey';
export const HOST = process.env.HOST || 'http://localhost:5000';

export const GOOGLE_CLIENT_ID = process.env.GOOGLE_CLIENT_ID;
export const GOOGLE_CLIENT_SECRET = process.env.GOOGLE_CLIENT_SECRET;
export const GOOGLE_REDIRECT_URL = process.env.GOOGLE_REDIRECT_URL;

export const MQTT_HOST = process.env.MQTT_HOST;
export const MQTT_PORT = process.env.MQTT_PORT;
export const MQTT_USERNAME = process.env.MQTT_USERNAME;
export const MQTT_PASSWORD = process.env.MQTT_PASSWORD;

export const DEVELOPMENT_CLIENT =
  NODE_ENV === 'development'
    ? 'https://oauth.glootie.ml'
    : 'http://localhost:3000';

export const SEND_IN_BLUE_API_KEY = process.env.SEND_IN_BLUE_API_KEY || '';

export const SCRAPER_USER_EMAIL =
  NODE_ENV === 'production' ? 'scraper@gmail.com' : 'test@gmail.com';

const MONGO = {
  host: MONGO_HOST,
  username: MONGO_USERNAME,
  password: MONGO_PASSWORD,
  url: `mongodb+srv://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOST}/${NODE_ENV}?retryWrites=true&w=majority`,
};

const MQTT = {
  host: MQTT_HOST,
  port: MQTT_PORT,
  username: MQTT_USERNAME,
  password: MQTT_PASSWORD,
};

const db = {
  mongo: MONGO,
  mqtt: MQTT,
};

export default db;
