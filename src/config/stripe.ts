import 'dotenv/config';

export const STRIPE_CLIENT_ID = process.env.STRIPE_CLIENT_ID;
export const STRIPE_SECRET_KEY = process.env.STRIPE_SECRET_KEY;
export const STRIPE_REDIRECT_URI = process.env.STRIPE_REDIRECT_URI;
export const STRIPE_AUTH_URI = 'https://connect.stripe.com/oauth/authorize';

const stripe = {
  env: {
    clientId: process.env.STRIPE_CLIENT_ID,
    secretKey: process.env.STRIPE_SECRET_KEY,
    redirectUri: process.env.STRIPE_REDIRECT_URI,
    // authorizationUri: 'https://connect.stripe.com/oauth/authorize', // Standard Accounts
    authorizationUri: 'https://connect.stripe.com/express/oauth/authorize', // Standard Accounts
  },
};

export default stripe;
