import StripeService from '.';
import Stripe from 'stripe';

export type CreatePriceDto = {
  unit_amount?: number;
  currency: string;
  recurring?: any;
  active?: boolean;
  nickname?: string;
  product?: string;
};

export type UpdatePriceDto = {
  unit_amount?: number;
  currency?: string;
  recurring?: any;
  active?: boolean;
  nickname?: string;
  product?: string;
};

export type PriceListParams = {
  limit: 3;
};

export default class PriceService extends StripeService {
  constructor(_secret: string) {
    super(_secret);
  }

  public async list(
    priceParams?: PriceListParams
  ): Promise<Stripe.Response<Stripe.ApiList<Stripe.Price>>> {
    const stripe = this.stripeClient();
    const price = await stripe.prices.list(priceParams);
    return price;
  }

  public async create(priceDto: CreatePriceDto): Promise<Stripe.Price> {
    const stripe = this.stripeClient();
    const price = await stripe.prices.create(priceDto);
    return price;
  }

  public async retrieve(priceId: string): Promise<Stripe.Price> {
    const stripe = this.stripeClient();
    const price = await stripe.prices.retrieve(priceId);
    return price;
  }

  public async update(
    priceId: string,
    priceDto: UpdatePriceDto
  ): Promise<Stripe.Price> {
    const stripe = this.stripeClient();
    const price = await stripe.prices.update(priceId, priceDto);
    return price;
  }
}
