import Stripe from 'stripe';

export default class StripeService {
  private secret: string;

  public constructor(_secret: string) {
    this.secret = _secret;
  }

  public stripeClient(): Stripe {
    const newStripe = new Stripe(this.secret, {
      apiVersion: '2022-08-01',
    });
    return newStripe;
  }
}
