import StripeService from '.';
import Stripe from 'stripe';

export type ProductDto = {
  name: string;
  images: string[];
  url: string;
};

export type ProductListParams = {
  limit: number;
};

export default class ProductService extends StripeService {
  constructor(_secret: string) {
    super(_secret);
  }

  public async list(
    productParams?: ProductListParams
  ): Promise<Stripe.Response<Stripe.ApiList<Stripe.Product>>> {
    const stripe = this.stripeClient();
    const product = await stripe.products.list(productParams);
    return product;
  }

  public async create(productDto: ProductDto): Promise<Stripe.Product> {
    const stripe = this.stripeClient();
    const product = await stripe.products.create(productDto);
    return product;
  }

  public async retrieve(productId: string): Promise<Stripe.Product> {
    const stripe = this.stripeClient();
    const product = await stripe.products.retrieve(productId);
    return product;
  }

  public async update(
    productId: string,
    productDto: ProductDto
  ): Promise<Stripe.Product> {
    const stripe = this.stripeClient();
    const product = await stripe.products.update(productId, productDto);
    return product;
  }

  public async delete(productId: string): Promise<boolean> {
    try {
      const stripe = this.stripeClient();
      await stripe.products.del(productId);
      return true;
    } catch (e) {
      // console.error(e);
      return false;
    }
  }
}
