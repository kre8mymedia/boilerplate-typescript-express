import mqtt from 'mqtt';
import db from '../config/config';

const host = db.mqtt.host;
const port = parseInt(db.mqtt.port);
const clientId = `mqtt_${Math.random().toString(16).slice(3)}`;
const connectUrl = `mqtt://${host}:${port}`;

class Broker {
  private client: mqtt.MqttClient;
  constructor() {
    this.client = mqtt.connect(connectUrl, {
      clientId,
      clean: false,
      connectTimeout: 4000,
      reconnectPeriod: 1000,
      username: db.mqtt.username,
      password: db.mqtt.password,
      //   rejectUnauthorized: false,
    });
  }

  publish(topic: string, message: string | Buffer) {
    try {
      this.client.publish(
        topic,
        message,
        { qos: 1, retain: false },
        (error) => {
          if (error) {
            console.error(error);
          }
        }
      );

      return true;
    } catch (e) {
      throw new Error(e.message());
    }
  }
}

export default Broker;
