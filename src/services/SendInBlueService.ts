import axios from 'axios';

const CONTACT_URL = 'https://api.sendinblue.com/v3/contacts';

export const REGISTERED_USER_LIST_ID = 3;

type Contact = {
  updateEnabled: boolean;
  email: string;
  listIds: number[];
};

export default class SendInBlueService {
  private secret: string;

  public constructor(_secret: string) {
    this.secret = _secret;
  }

  public async createContact(contact: Contact) {
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'api-key': this.secret,
    };
    try {
      const res = await axios.post(CONTACT_URL, contact, { headers });
      return res;
    } catch (e) {
      console.log(e);
      if (e.response.data.code === 'duplicate_parameter') {
        alert(e.response.data.message);
      }
    }
  }

  public async deleteContact(id: number) {
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'api-key': this.secret,
    };
    try {
      const res = await axios.delete(`${CONTACT_URL}/${id}`, { headers });
      return res;
    } catch (e) {
      throw new Error(e);
    }
  }
}
