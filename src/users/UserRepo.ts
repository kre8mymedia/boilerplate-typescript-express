import User from './User';
import IUser, { TokenData } from '../users/UserInterface';

export default class UserRepo {
  public async findById(id: string): Promise<IUser> {
    try {
      const model = await User.findById(id);
      return model;
    } catch (e) {
      console.log(e);
    }
  }

  public async authUser(tokenData: TokenData): Promise<IUser> {
    try {
      const model = await User.findById(tokenData._id);
      return model;
    } catch (e) {
      console.log(e);
    }
  }
}
