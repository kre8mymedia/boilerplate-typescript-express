import IUser from './UserInterface';
import mongoose, { Schema } from 'mongoose';
import { passwordHash } from '../utils/auth';

const UserSchema: Schema = new Schema(
  {
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    username: {
      type: String,
      required: true,
      index: { unique: true, dropDups: true },
    },
    email: {
      type: String,
      required: true,
      index: { unique: true, dropDups: true },
    },
    password: { type: String },
    googleId: { type: String },
    organization: { type: Schema.Types.ObjectId, ref: 'Organization' },
  },
  {
    timestamps: true,
    toObject: { versionKey: false },
  }
);

UserSchema.set('versionKey', false);

UserSchema.pre('save', async function (next) {
  if (this.isModified('password')) {
    this.password = passwordHash(this.password);
  }
  next();
});

export default mongoose.model<IUser>('User', UserSchema);
