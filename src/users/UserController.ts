import { Request, Response } from 'express';
import mongoose from 'mongoose';
import User from '../users/User';

const list = async (req: Request, res: Response) => {
  return res.status(401).json({
    success: false,
    message: 'Unauthorized',
  });

  User.find()
    .exec()
    .then((results) => {
      return res.status(200).json({
        users: results,
        count: results.length,
      });
    })
    .catch((err: Error) => {
      return res.status(500).json({
        message: err.message,
        err,
      });
    });
};

const create = async (req: Request, res: Response) => {
  const { firstname, lastname, username, email, password } = req.body;

  const user = new User({
    _id: new mongoose.Types.ObjectId(),
    firstname,
    lastname,
    username,
    email,
    password,
  });

  try {
    const result = await user.save();
    delete result.password;
    return res.status(201).json({
      user: result,
    });
  } catch (err) {
    return res.status(500).json({
      message: err.message,
      err,
    });
  }
};

const find = async (req: Request, res: Response) => {
  try {
    const user = await User.findById(req.params.id).exec();
    if (!user) {
      return res.status(404).json({
        success: false,
      });
    } else {
      return res.status(200).json({
        user: user,
      });
    }
  } catch (err) {
    return res.status(500).json({
      message: err.message,
      err,
    });
  }
};

const update = async (req: Request, res: Response) => {
  const { firstname, lastname, username, email, password } = req.body;

  try {
    const user = await User.findById(req.params.id);

    if (!user) {
      return res.status(404).json({
        success: false,
      });
    } else {
      user.firstname = firstname ? firstname : null;
      user.lastname = lastname ? lastname : null;
      user.username = username ? username : null;
      user.email = email ? email : null;
      user.password = password ? password : null;
      await user.save();

      return res.status(200).json({
        user: user,
      });
    }
  } catch (err) {
    return res.status(500).json({
      message: err.message,
      err,
    });
  }
};

const remove = async (req: Request, res: Response) => {
  try {
    const user = await User.findByIdAndDelete(req.params.id).exec();
    if (!user) {
      return res.status(404).json({
        success: false,
      });
    } else {
      return res.status(200).json({
        success: true,
      });
    }
  } catch (err) {
    return res.status(500).json({
      message: err.message,
      err,
    });
  }
};

export default { list, create, find, update, remove };
