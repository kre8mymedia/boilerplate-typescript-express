/**
 * --------------------------------------------------------------------------------------
 * Timestamp Schema
 * --------------------------------------------------------------------------------------
 * @openapi
 * components:
 *   schemas:
 *     Timestamps:
 *       type: object
 *       properties:
 *         createdAt:
 *           type: timestamp
 *           description: When created
 *           example: 2022-05-16T16:00:00.000Z
 *         updatedAt:
 *           type: timestamp
 *           description: Last update
 *           example: 2022-05-16T16:51:40.000Z
 */

/**
 * --------------------------------------------------------------------------------------
 * User Schema
 * --------------------------------------------------------------------------------------
 * @openapi
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       properties:
 *         _id:
 *           type: uid
 *           description: The user ID.
 *           example: 6301891496aa2cb5c48e1c93
 *         firstName:
 *           type: string
 *           description: The user's name.
 *           example: Leanne
 *         lastName:
 *           type: string
 *           description: User last name
 *           example: Graham
 *         userName:
 *           type: string
 *           description: The username.
 *           example: Gamer1234
 *         email:
 *           type: string
 *           description: User email
 *           example: 'john@testcom'
 */

/**
 * --------------------------------------------------------------------------------------
 * Fetch User by ID
 * --------------------------------------------------------------------------------------
 * @openapi
 * /users/{id}:
 *   get:
 *     tags:
 *       - Users
 *     summary: Retreive User by ID
 *     description: retrieve user by id
 *     responses:
 *       200:
 *         description: A single user.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  user:
 *                      type: object
 *                      $ref: '#/components/schemas/User'
 */

/**
 * --------------------------------------------------------------------------------------
 * Update User by ID
 * --------------------------------------------------------------------------------------
 * @openapi
 * /users/{id}:
 *   put:
 *     tags:
 *       - Users
 *     summary: Update User by ID
 *     description: retrieve user by id
 *     responses:
 *       201:
 *         description: Update single user.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  user:
 *                      type: object
 *                      $ref: '#/components/schemas/User'
 */
