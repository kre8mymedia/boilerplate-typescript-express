import express from 'express';
import userController from './UserController';

import { authMiddleware } from '../middleware/auth';

const router = express.Router();

// router.get('/users', userController.list);
// router.post('/users', userController.create);
router.get('/users/:id', authMiddleware, userController.find);
router.put('/users/:id', authMiddleware, userController.update);
// router.delete('/users/:id', authMiddleware, userController.remove);

export default router;
