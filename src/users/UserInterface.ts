import { Document } from 'mongoose';

export default interface IUser extends Document {
  firstname: string;
  lastname: string;
  username: string;
  email: string;
  password: string;
  organization: string;
}

export type RegisterInput = {
  firstname: string;
  lastname: string;
  username: string;
  email: string;
  password: string;
};

export type LoginInput = {
  email: string;
  password: string;
};

export type TokenData = {
  _id: string;
  email: string;
  iat: number;
  exp: number;
};
