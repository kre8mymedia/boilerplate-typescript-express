import swaggerDocs from './utils/swagger';
import express, { Request, Response, NextFunction } from 'express';
// Database
import dbConfig, {
  APP_SECRET,
  NODE_ENV,
  DEVELOPMENT_CLIENT,
} from './config/config';
import mongoose from 'mongoose';
// Auth
import session from 'express-session';
import passport from 'passport';
import cors from 'cors';
import './config/passport';
// Routes
import routes from './routes/api';
import paymentRoutes from './routes/stripe';

const app = express();
const port = 5000;
const dbPort = 27017;

function isLoggedIn(req: Request, res: Response, next: NextFunction) {
  req.user ? next() : res.sendStatus(401);
}

/**------------------------------------------------------------------
 *  Database
 * ------------------------------------------------------------------
 */
mongoose
  .connect(dbConfig.mongo.url)
  .then(() => {
    if (NODE_ENV !== 'test') {
      app.listen(dbPort, () =>
        console.log(
          `Server running on port ${port}\nDatabase running on port ${dbPort}`
        )
      );
    }
  })
  .catch((err: Error) => console.log(err));

/**------------------------------------------------------------------
 *  Utils
 * ------------------------------------------------------------------
 */
const corsOption = {
  origin: [
    'https://dev.skrumify.com',
    'https://my.skrumify.com',
    'http://localhost:3000',
    'http://localhost:1234',
    'https://calendar.glootie.ml',
    'https://dev-calendar.glootie.ml',
    DEVELOPMENT_CLIENT,
  ],
  credentials: true,
};
app.use(express.json());
app.use(cors(corsOption));
app.use(
  session({
    secret: APP_SECRET,
    resave: true,
    saveUninitialized: true,
  })
);
app.use(passport.initialize());
app.use(passport.session());

/**------------------------------------------------------------------
 *  Page Routes
 * ------------------------------------------------------------------
 */
app.get('/', (_, res) => {
  res
    .status(200)
    .send(
      '<h1>Typescript Express Mongo</h2><br/><a href="/docs">API Docs</a> <a href="/auth/google">Google Login</a> <a href="/stripe/auth">Stripe Connect</a>'
    );
});

app.get('/about', (_, res) => {
  res.status(200).send('<h1>About Page</h2>');
});

app.get('/contact', (_, res) => {
  res.status(200).send('<h1>Contact Page</h2>');
});

/**------------------------------------------------------------------
 * Google OAuth2.0 Routes
 * ------------------------------------------------------------------
 */
app.get(
  '/auth/google',
  passport.authenticate('google', {
    scope: ['email', 'profile'],
  })
);

app.get(
  '/auth/google/callback',
  passport.authenticate('google', {
    failureRedirect: '/login',
  }),
  function (req, res) {
    res.redirect(DEVELOPMENT_CLIENT);
  }
);

app.get('/auth/failure', (req, res) => {
  res.send('something went wrong...');
});

app.get('/protected', isLoggedIn, (req, res) => {
  console.log(req.session);
  res.redirect('back');
});

app.get('/getuser', (req, res) => {
  res.send(req.user);
});

app.get('/auth/logout', function (req, res) {
  if (req.user) {
    req.session.destroy(function (err: Error) {
      if (err) {
        res.send(err);
      }
      res.send('done'); //Inside a callback… bulletproof!
    });
  }
});

/**------------------------------------------------------------------
 *  API Routes
 * ------------------------------------------------------------------
 */
app.use('/api/v1', routes);
app.use('/stripe', paymentRoutes);

if (NODE_ENV !== 'test') {
  app.listen(port, () => {
    // console.log(`App running on port ${port}`);
    swaggerDocs(app);
  });
}

export default app;
