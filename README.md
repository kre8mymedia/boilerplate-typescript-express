# Boilerplate - Typescript Express Mongoose

## Helpful Links
- [Express Swagger Docs](https://dev.to/kabartolo/how-to-document-an-express-api-with-swagger-ui-and-jsdoc-50do)
- [Events Calendar Demo](https://l6tlsg.csb.app/)

### Set and edit .env file
```bash
cp .example.env .env
```

### Build Proejct
```bash
docker-compose up --build
```

### Run Tests
```bash
## Run both unit & integration
npm run test

## Unit tests
npm run test:unit

## Integration tests
npm run test:integration
```
