#!/bin/bash

if [ -z "$1" ]
then
      echo "ERROR: First argument for APP_ENV is empty"
      exit
fi

if [ -z "$2" ]
then
      echo "ERROR: Second argument for NAMESPACE is empty"
      exit
fi

if [ -z "$3" ]
then
      echo "ERROR: Third argument for TAG is empty"
      exit
fi

### Set Environment Variables
set -a # automatically export all variables
source .env.production
set +a

helm -n $2 upgrade -i --debug --wait --atomic \
--set appEnv=$1 \
--set image.repository=$3 \
--set host=$HOST \
--set dbHost=$MONGO_HOST \
--set dbUser=$MONGO_USERNAME \
--set dbPassword=$MONGO_PASSWORD \
--set mqttHost=$MQTT_HOST \
--set mqttPort=$MQTT_PORT \
--set mqttUser=$MQTT_USERNAME \
--set mqttPassword=$MQTT_PASSWORD \
--set googleClientId=$GOOGLE_CLIENT_ID \
--set googleClientSecret=$GOOGLE_CLIENT_SECRET \
--set stripeClientId=$STRIPE_CLIENT_ID \
--set stripeSecretKey=$STRIPE_SECRET_KEY \
--set stripeRedirectUri=$STRIPE_REDIRECT_URI \
--set sendInBlueApiKey=$SEND_IN_BLUE_API_KEY \
server ./k8s/helm

echo ""
echo ""

kubectl -n $2 get all